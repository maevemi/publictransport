use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::StringObject;
use gtk::{glib, CompositeTemplate};
use hafas_client::{Journey, Location};
use shumate::prelude::*;
use shumate::Coordinate;

use crate::models::{journey::HafasJourney, location_marker::LocationMarker};

mod imp {
    use super::*;
    use glib::{ParamSpec, ParamSpecObject, Value};
    use once_cell::sync::Lazy;
    use std::cell::RefCell;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/maevemi/publictransport/map_view.ui")]
    pub struct MapView {
        #[template_child]
        pub map: TemplateChild<shumate::SimpleMap>,
        #[template_child]
        pub map_license: TemplateChild<shumate::License>,
        pub marker_layer: RefCell<shumate::MarkerLayer>,
        pub path_layer: RefCell<shumate::PathLayer>,
        pub details_journey: RefCell<HafasJourney>,
        pub title: RefCell<Option<StringObject>>,
        pub location_marker: RefCell<LocationMarker>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for MapView {
        const NAME: &'static str = "MapView";
        type Type = super::MapView;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for MapView {
        fn constructed(&self) {
            let obj = self.obj();
            self.parent_constructed();
            obj.setup_map();

            obj.connect_notify_local(Some("details-journey"), move |window, _| {
                let journey_option = window.property::<HafasJourney>("details-journey").get();
                match journey_option {
                    Some(journey) => window.set_map_details(&journey),
                    None => (),
                }
            });

            obj.connect_notify_local(Some("location-marker"), move |window, _| {
                let (place, marker_type) =
                    window.property::<LocationMarker>("location-marker").get();
                window.show_location(marker_type, &place);
            });
        }

        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecObject::builder::<HafasJourney>("details-journey").build(),
                    ParamSpecObject::builder::<StringObject>("title").build(),
                    ParamSpecObject::builder::<LocationMarker>("location-marker").build(),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "details-journey" => {
                    let details_journey = value
                        .get()
                        .expect("The value needs to be of type `HafasJourney`.");
                    self.details_journey.replace(details_journey);
                }
                "title" => {
                    let title = value
                        .get()
                        .expect("The value needs to be of type `StringObject`.");
                    self.title.replace(title);
                }
                "location-marker" => {
                    let location_marker = value
                        .get()
                        .expect("The value needs to be of type `HafasStationboard`.");
                    self.location_marker.replace(location_marker);
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "details-journey" => self.details_journey.borrow().to_value(),
                "title" => self.title.borrow().to_value(),
                "location-marker" => self.location_marker.borrow().to_value(),
                _ => unimplemented!(),
            }
        }
    }
    impl WidgetImpl for MapView {}
    impl BoxImpl for MapView {}
}

glib::wrapper! {
    pub struct MapView(ObjectSubclass<imp::MapView>)
    @extends gtk::Widget, gtk::Box,
    @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Orientable;
}

impl MapView {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[])
    }

    pub fn setup_map(&self) {
        let registry = shumate::MapSourceRegistry::with_defaults();
        let source = registry.by_id(&shumate::MAP_SOURCE_OSM_MAPNIK).unwrap();
        let viewport = self.imp().map.viewport().unwrap();
        viewport.set_reference_map_source(Some(&source));
        viewport.set_zoom_level(5.0);

        let layer = shumate::MapLayer::new(&source, &viewport);
        self.imp().map.add_overlay_layer(&layer);

        let path_layer = shumate::PathLayer::new(&viewport);
        self.imp().map.add_overlay_layer(&path_layer);
        *self.imp().path_layer.borrow_mut() = path_layer;

        let marker_layer = shumate::MarkerLayer::new(&viewport);
        self.imp().map.add_overlay_layer(&marker_layer);
        *self.imp().marker_layer.borrow_mut() = marker_layer;

        self.imp().map_license.append_map_source(&source);
        self.imp().map.license().unwrap().hide();
    }

    pub fn create_marker(&self, marker_type: MarkerType) -> shumate::Marker {
        let marker_layer = &*self.imp().marker_layer.borrow();

        let marker_img = match marker_type {
            MarkerType::Origin => gtk::Image::from_icon_name("first-symbolic"),
            MarkerType::Stopover => gtk::Image::from_icon_name("exchange-symbolic"),
            MarkerType::Destination => gtk::Image::from_icon_name("last-symbolic"),
        };

        if adw::StyleManager::default().is_dark() {
            marker_img.add_css_class("circle-dark");
        } else {
            marker_img.add_css_class("circle");
        }
        marker_img.set_icon_size(gtk::IconSize::Normal);

        let marker = shumate::Marker::new();
        marker.set_child(Some(&marker_img));
        marker_layer.add_marker(&marker);
        marker
    }

    pub fn show_location(&self, marker_type: MarkerType, place: &Location) {
        let path_layer = &*self.imp().path_layer.borrow();
        let marker_layer = &*self.imp().marker_layer.borrow();
        let viewport = self.imp().map.viewport().unwrap();

        path_layer.remove_all();
        marker_layer.remove_all();

        let latitude = place.coordinates.latitude;
        let longitude = place.coordinates.longitude;
        self.create_marker(marker_type)
            .set_location(latitude, longitude);
        viewport.set_location(latitude, longitude);
        viewport.set_zoom_level(15.0);

        self.set_property("title", Some(StringObject::new(&place.name)));
    }

    pub fn set_map_details(&self, journey: &Journey) {
        let origin = journey.legs[0].origin.name.clone();
        let destination = &journey
            .legs
            .last()
            .expect("Connection part not found")
            .destination
            .name;
        let mut origin_destination = origin;
        origin_destination.push_str(&" -> ".to_string());
        origin_destination.push_str(&destination);
        self.set_property("title", Some(StringObject::new(&origin_destination)));

        let path_layer = &*self.imp().path_layer.borrow();
        let marker_layer = &*self.imp().marker_layer.borrow();
        let viewport = self.imp().map.viewport().unwrap();

        path_layer.remove_all();
        marker_layer.remove_all();

        path_layer.add_node(&Coordinate::new_full(
            journey.legs[0].origin.coordinates.latitude,
            journey.legs[0].origin.coordinates.longitude,
        ));
        self.create_marker(MarkerType::Origin).set_location(
            journey.legs[0].origin.coordinates.latitude,
            journey.legs[0].origin.coordinates.longitude,
        );
        for leg in journey.legs.iter() {
            if let Some(stopovers) = &leg.stopovers {
                for stopover in stopovers.iter() {
                    path_layer.add_node(&Coordinate::new_full(
                        stopover.stop.coordinates.latitude,
                        stopover.stop.coordinates.longitude,
                    ));
                }
            }
            path_layer.add_node(&Coordinate::new_full(
                leg.destination.coordinates.latitude,
                leg.destination.coordinates.longitude,
            ));
            self.create_marker(MarkerType::Stopover).set_location(
                leg.destination.coordinates.latitude,
                leg.destination.coordinates.longitude,
            );
        }
        self.create_marker(MarkerType::Destination).set_location(
            journey
                .legs
                .last()
                .unwrap()
                .destination
                .coordinates
                .latitude,
            journey
                .legs
                .last()
                .unwrap()
                .destination
                .coordinates
                .longitude,
        );

        let nodes = path_layer.nodes();
        let latitude_max = nodes
            .iter()
            .map(|n| n.latitude())
            .fold(f64::NEG_INFINITY, f64::max);
        let latitude_min = nodes
            .iter()
            .map(|n| n.latitude())
            .fold(f64::INFINITY, f64::min);
        let longitude_max = nodes
            .iter()
            .map(|n| n.longitude())
            .fold(f64::NEG_INFINITY, f64::max);
        let longitude_min = nodes
            .iter()
            .map(|n| n.longitude())
            .fold(f64::INFINITY, f64::min);
        viewport.set_location(
            (latitude_max + latitude_min) / 2_f64,
            (longitude_max + longitude_min) / 2_f64,
        );

        // calculate distance according to https://de.wikipedia.org/wiki/Orthodrome
        let zeta = (latitude_max.to_radians().sin() * latitude_min.to_radians().sin()
            + latitude_max.to_radians().cos()
                * latitude_min.to_radians().cos()
                * (longitude_max - longitude_min).to_radians().cos())
        .acos();
        let distance = zeta * 6370000.0;

        let resolution = distance / 300.0;

        // calculate zoomlevel according to https://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#Resolution_and_Scale
        let zoomlevel = (156543.03 * ((latitude_max + latitude_min) / 2.0).to_radians().cos()
            / resolution)
            .log2();

        viewport.set_zoom_level(zoomlevel * 0.98);

        if adw::StyleManager::default().is_dark() {
            path_layer.set_stroke_color(Some(&gtk::gdk::RGBA::new(
                32.0 / 255.0,
                74.0 / 255.0,
                135.0 / 255.0,
                1.0,
            )));
        } else {
            path_layer.set_stroke_color(Some(&gtk::gdk::RGBA::new(
                165.0 / 255.0,
                201.0 / 255.0,
                240.0 / 255.0,
                1.0,
            )));
        }
        path_layer.set_stroke_width(5.0);
        path_layer.set_stroke(true);
    }
}

#[derive(Debug, Clone)]
pub enum MarkerType {
    Origin,
    Stopover,
    Destination,
}
