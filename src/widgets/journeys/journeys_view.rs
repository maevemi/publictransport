use glib::clone;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};
use hafas_client::{BoxError, Journeys};
use log::{error, warn};

use crate::models::{client::HafasClient, journey::HafasJourney};
use crate::widgets::journeys::journey_row::JourneyRow;

mod imp {
    use super::*;
    use glib::{ParamSpec, ParamSpecObject, Value};
    use once_cell::sync::Lazy;
    use std::cell::RefCell;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/maevemi/publictransport/journeys_view.ui")]
    pub struct JourneysView {
        #[template_child]
        pub list_view: TemplateChild<crate::widgets::list_view::ListView>,
        pub client: RefCell<HafasClient>,
        pub details_journey: RefCell<HafasJourney>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for JourneysView {
        const NAME: &'static str = "JourneysView";
        type Type = super::JourneysView;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for JourneysView {
        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecObject::builder::<HafasClient>("client").build(),
                    ParamSpecObject::builder::<HafasJourney>("details-journey").build(),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "client" => {
                    let client = value
                        .get()
                        .expect("The value needs to be of type `HafasClient`.");
                    self.client.replace(client);
                }
                "details-journey" => {
                    let details_journey = value
                        .get()
                        .expect("The value needs to be of type `HafasJourney`.");
                    self.details_journey.replace(details_journey);
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "client" => self.client.borrow().to_value(),
                "details-journey" => self.details_journey.borrow().to_value(),
                _ => unimplemented!(),
            }
        }

        fn constructed(&self) {
            let obj = self.obj();
            obj.setup_signals();

            self.parent_constructed();
        }
    }
    impl WidgetImpl for JourneysView {}
    impl BoxImpl for JourneysView {}
}

glib::wrapper! {
    pub struct JourneysView(ObjectSubclass<imp::JourneysView>)
        @extends gtk::Widget, gtk::Box,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Orientable;
}

impl JourneysView {
    pub fn new() -> Self {
        let journeys_view: JourneysView = glib::Object::new::<JourneysView>(&[]);

        journeys_view
    }

    pub fn setup_signals(&self) {
        self.connect_notify_local(Some("client"), move |journeys_view, _| {
            let client = journeys_view.imp().client.borrow().get();

            if client.get_departure_id().is_some() && client.get_arrival_id().is_some() {
                journeys_view.journeys();
            }
        });

        let list_view = self.imp().list_view.imp();

        list_view.earlier.connect_clicked(clone!(
            @weak self as journeys_view
            => move |_| {
            journeys_view.earlier();
        }));

        list_view.later.connect_clicked(clone!(
            @weak self as journeys_view
            => move |_| {
            journeys_view.later();
        }));

        list_view.listbox.connect_row_activated(
            clone!(@weak self as journeys_view => move |_list, row| {
                let row = row.downcast_ref::<JourneyRow>().unwrap();
                let hafas_journey = &*row.imp().journey.borrow();
                journeys_view.set_property("details-journey", hafas_journey);
            }),
        );
    }

    pub fn journeys(&self) {
        self.imp().list_view.show_spinner();

        let hafas_client = &*self.imp().client.borrow();
        let client = hafas_client.get().clone();

        let journeys_handle: tokio::task::JoinHandle<Result<Journeys, BoxError>> = crate::RUNTIME
            .spawn(async move {
                let journeys = client.get_journeys().await;
                // std::thread::sleep(std::time::Duration::from_secs(2));
                journeys
            });

        gtk_macros::spawn!(clone!(
        @weak self as journeys_view
        => async move {
            match journeys_handle.await {
                Ok(journeys_handle_inner) => match journeys_handle_inner {
                    Ok(journeys) => {
                        journeys_view.imp().list_view.show_all_but_spinner();
                        let _ = journeys_view.activate_action("win.show-journeys-refresh", None);
                        for journey in journeys.journeys.iter() {
                            let row = JourneyRow::new(&journey);
                            journeys_view.imp().list_view.imp().listbox.get().append(&row);
                        }
                    },
                    Err(e) => {
                        warn!("Error getting journeys: {:#?}", e);
                        journeys_view.imp().list_view.hide_spinner();
                        let _ = journeys_view.activate_action("win.hide-journeys-refresh", None);
                        journeys_view.show_toast("No Journeys found");
                    },
                },
                Err(e) => {
                    journeys_view.show_toast("internal Error getting Journeys");
                    error!("Handle error: {:#?}", e);
                    journeys_view.imp().list_view.hide_spinner();
                    let _ = journeys_view.activate_action("win.hide-journeys-refresh", None);
                },
            };
        }));
    }

    pub fn earlier(&self) {
        self.imp().list_view.show_earlier_spinner();

        let hafas_client = &*self.imp().client.borrow();
        let client = hafas_client.get().clone();

        let journeys_handle: tokio::task::JoinHandle<Result<Journeys, BoxError>> = crate::RUNTIME
            .spawn(async move {
                let journeys = client.get_earlier_journeys().await;
                // std::thread::sleep(std::time::Duration::from_secs(2));
                journeys
            });

        gtk_macros::spawn!(clone!(
        @weak self as journeys_view
        => async move {
            match journeys_handle.await {
                Ok(journeys_handle_inner) => match journeys_handle_inner {
                    Ok(mut journeys) => {
                        journeys_view.imp().list_view.hide_earlier_spinner();
                        journeys.journeys.reverse();
                        for journey in journeys.journeys.iter() {
                            let row = JourneyRow::new(&journey);
                            journeys_view.imp().list_view.imp().listbox.get().prepend(&row);
                        }
                    },
                    Err(e) => {
                        warn!("Error getting earlier journeys: {:#?}", e);
                        journeys_view.imp().list_view.hide_earlier_spinner();
                        journeys_view.show_toast("Error getting earlier Journeys");
                    },
                },
                Err(e) => {
                    error!("Handle error: {:#?}", e);
                    journeys_view.imp().list_view.hide_earlier_spinner();
                    journeys_view.show_toast("internal Error getting earlier Journeys");
                },
            };
        }));
    }

    pub fn later(&self) {
        self.imp().list_view.show_later_spinner();

        let hafas_client = &*self.imp().client.borrow();
        let client = hafas_client.get().clone();

        let journeys_handle: tokio::task::JoinHandle<Result<Journeys, BoxError>> = crate::RUNTIME
            .spawn(async move {
                let journeys = client.get_later_journeys().await;
                // std::thread::sleep(std::time::Duration::from_secs(2));
                journeys
            });

        gtk_macros::spawn!(clone!(
        @weak self as journeys_view
        => async move {
            match journeys_handle.await {
                Ok(journeys_handle_inner) => match journeys_handle_inner {
                    Ok(journeys) => {
                        journeys_view.imp().list_view.hide_later_spinner();
                        for journey in journeys.journeys.iter() {
                            let row = JourneyRow::new(&journey);
                            journeys_view.imp().list_view.imp().listbox.get().append(&row);
                        }
                    },
                    Err(e) => {
                        warn!("Error getting later journeys: {:#?}", e);
                        journeys_view.imp().list_view.hide_later_spinner();
                        journeys_view.show_toast("Error getting later Journeys");
                    },
                },
                Err(e) => {
                    error!("Handle error: {:#?}", e);
                    journeys_view.imp().list_view.hide_later_spinner();
                    journeys_view.show_toast("internal Error getting later Journeys");
                },
            };
        }));
    }

    pub fn show_toast(&self, text: &str) {
        self.imp().list_view.show_toast(text);
    }
}
