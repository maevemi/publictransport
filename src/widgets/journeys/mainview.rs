use glib::clone;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};

use crate::models::products_used::ProductsUsedModel;

mod imp {
    use super::*;
    use crate::models::client::HafasClient;
    use crate::models::location_id::LocationId;
    use glib::{ParamSpec, ParamSpecObject, Value};
    use once_cell::sync::Lazy;
    use std::cell::RefCell;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/maevemi/publictransport/journeys_mainview.ui")]
    pub struct JourneysMainView {
        // Template widgets
        #[template_child]
        pub departure_place: TemplateChild<crate::widgets::locationsearch::LocationSearch>,
        #[template_child]
        pub arrival_place: TemplateChild<crate::widgets::locationsearch::LocationSearch>,
        #[template_child]
        pub stopover_place: TemplateChild<crate::widgets::locationsearch::LocationSearch>,
        #[template_child]
        pub stopover_row: TemplateChild<gtk::Box>,
        #[template_child]
        pub add_stopover: TemplateChild<gtk::Button>,
        #[template_child]
        pub route_reverse: TemplateChild<gtk::Button>,
        #[template_child]
        pub remove_stopover: TemplateChild<gtk::Button>,
        #[template_child]
        pub journeys_view: TemplateChild<crate::widgets::journeys::journeys_view::JourneysView>,
        #[template_child]
        pub date_time: TemplateChild<crate::widgets::datetime::button::DatetimeButton>,
        #[template_child]
        pub refresh_journeys: TemplateChild<gtk::Button>,
        #[template_child]
        pub transportation_means: TemplateChild<gtk::Button>,
        #[template_child]
        pub transportation_means_image: TemplateChild<gtk::Image>,
        pub products_used_popover: crate::widgets::products_used_popover::ProductsUsed,
        pub client: RefCell<HafasClient>,
        pub departure_id: RefCell<LocationId>,
        pub arrival_id: RefCell<LocationId>,
        pub stopover_id: RefCell<LocationId>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for JourneysMainView {
        const NAME: &'static str = "PublictransportJourneysMainView";
        type Type = super::JourneysMainView;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);

            klass.install_action("win.show-journeys-refresh", None, |obj, _, _| {
                obj.imp().refresh_journeys.get().set_visible(true);
            });

            klass.install_action("win.hide-journeys-refresh", None, |obj, _, _| {
                obj.imp().refresh_journeys.get().set_visible(false);
            });

            klass.install_action("win.add-stopover", None, |obj, _, _| {
                obj.imp().stopover_row.set_visible(true);
                obj.action_set_enabled("win.add-stopover", false);
            });
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for JourneysMainView {
        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecObject::builder::<HafasClient>("client").build(),
                    ParamSpecObject::builder::<LocationId>("departure-id").build(),
                    ParamSpecObject::builder::<LocationId>("arrival-id").build(),
                    ParamSpecObject::builder::<LocationId>("stopover-id").build(),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "client" => {
                    let client = value
                        .get()
                        .expect("The value needs to be of type `HafasClient`.");
                    self.client.replace(client);
                }
                "departure-id" => {
                    let departure_id = value
                        .get()
                        .expect("The value needs to be of type `LocationId`.");
                    self.departure_id.replace(departure_id);
                }
                "arrival-id" => {
                    let arrival_id = value
                        .get()
                        .expect("The value needs to be of type `LocationId`.");
                    self.arrival_id.replace(arrival_id);
                }
                "stopover-id" => {
                    let stopover_id = value
                        .get()
                        .expect("The value needs to be of type `LocationId`.");
                    self.stopover_id.replace(stopover_id);
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "client" => self.client.borrow().to_value(),
                "departure-id" => self.departure_id.borrow().to_value(),
                "arrival-id" => self.arrival_id.borrow().to_value(),
                "stopover-id" => self.stopover_id.borrow().to_value(),
                _ => unimplemented!(),
            }
        }

        fn constructed(&self) {
            let obj = self.obj();
            self.parent_constructed();

            obj.bind_property("client", &obj.imp().journeys_view.get(), "client")
                .build();

            obj.bind_property("client", &obj.imp().products_used_popover, "products-used")
                .transform_to(|_, value: HafasClient| {
                    let client = value.get();
                    let products_used =
                        ProductsUsedModel::new(&client.journeys_config.products_used).get();
                    Some(ProductsUsedModel::new(&products_used).to_value())
                })
                .transform_from(|binding, value: ProductsUsedModel| {
                    let products_used = value.get();
                    let client = binding
                        .source()
                        .unwrap()
                        .property::<HafasClient>("client")
                        .get();
                    for (i, product_used) in products_used.iter().enumerate() {
                        *client.journeys_config.products_used[i].used.lock().unwrap() =
                            *product_used.used.lock().unwrap();
                    }
                    Some(HafasClient::new(&client).to_value())
                })
                .bidirectional()
                .build();

            obj.imp()
                .arrival_place
                .get()
                .imp()
                .popover
                .bind_property("selected-location-id", &*obj, "arrival-id")
                .build();

            obj.imp()
                .departure_place
                .get()
                .imp()
                .popover
                .bind_property("selected-location-id", &*obj, "departure-id")
                .build();

            obj.imp()
                .stopover_place
                .get()
                .imp()
                .popover
                .bind_property("selected-location-id", &*obj, "stopover-id")
                .build();

            obj.connect_notify_local(Some("departure-id"), move |journeys_main_view, _| {
                let departure_id = journeys_main_view
                    .property::<LocationId>("departure-id")
                    .get();
                let hafas_client = journeys_main_view.property::<HafasClient>("client").get();
                let hafas_client_new =
                    HafasClient::new(hafas_client.set_departure_id(&departure_id));
                journeys_main_view.set_property("client", hafas_client_new);
            });

            obj.connect_notify_local(Some("arrival-id"), move |journeys_main_view, _| {
                let arrival_id = journeys_main_view
                    .property::<LocationId>("arrival-id")
                    .get();
                let hafas_client = journeys_main_view.property::<HafasClient>("client").get();
                let hafas_client_new = HafasClient::new(hafas_client.set_arrival_id(&arrival_id));
                journeys_main_view.set_property("client", hafas_client_new);
            });

            obj.connect_notify_local(Some("stopover-id"), move |journeys_main_view, _| {
                let stopover_id = journeys_main_view
                    .property::<LocationId>("stopover-id")
                    .get();
                let hafas_client = journeys_main_view.property::<HafasClient>("client").get();
                let hafas_client_new =
                    HafasClient::new(hafas_client.set_journeys_via(&stopover_id));
                journeys_main_view.set_property("client", hafas_client_new);
            });

            obj.connect_notify_local(Some("client"), move |journeys_main_view, _| {
                let client = journeys_main_view.property::<HafasClient>("client").get();
                let products_used = client.journeys_config.products_used.to_vec();

                for product_used in products_used.iter() {
                    let product_used_inner = *product_used.used.lock().unwrap();
                    if product_used_inner == false {
                        journeys_main_view
                            .imp()
                            .transportation_means_image
                            .set_icon_name(Some("bus-symbolic-circle"));
                        break;
                    }
                    journeys_main_view
                        .imp()
                        .transportation_means_image
                        .set_icon_name(Some("bus-symbolic"));
                }
            });

            let journeys_view = self.journeys_view.clone();
            self.refresh_journeys.connect_clicked(move |_| {
                journeys_view.journeys();
            });

            self.remove_stopover
                .connect_clicked(clone!(@strong obj => move |_| {
                    obj.imp().stopover_row.set_visible(false);
                    obj.action_set_enabled("win.add-stopover", true);
                    obj.imp().stopover_place.set_text("".to_string());
                    obj.set_property("stopover-id", LocationId::new(None));
                }));

            self.route_reverse
                .connect_clicked(clone!(@strong obj => move |_| {
                    let origin = obj.imp().arrival_place.text();
                    let destination = obj.imp().departure_place.text();
                    obj.imp().arrival_place.set_text(destination);
                    obj.imp().departure_place.set_text(origin);

                    let hafas_client = obj
                        .property::<HafasClient>("client")
                        .get();
                    let arrival_id = hafas_client.get_arrival_id();
                    let departure_id = hafas_client.get_departure_id();

                    hafas_client.set_arrival_id(&departure_id);
                    hafas_client.set_departure_id(&arrival_id);

                    let hafas_client_new =
                        HafasClient::new(&hafas_client);
                    obj.set_property("client", hafas_client_new);
                }));

            self.products_used_popover
                .set_parent(&self.transportation_means.get());
            let products_used_popover = self.products_used_popover.clone();
            self.transportation_means.connect_clicked(move |_| {
                products_used_popover.popup();
            });
        }
    }
    impl WidgetImpl for JourneysMainView {}
    impl BoxImpl for JourneysMainView {}
}

glib::wrapper! {
    pub struct JourneysMainView(ObjectSubclass<imp::JourneysMainView>)
        @extends gtk::Widget, gtk::Box,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Orientable;
}

impl JourneysMainView {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[])
    }
}
