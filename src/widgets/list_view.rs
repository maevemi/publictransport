use gettextrs::gettext;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/maevemi/publictransport/list_view.ui")]
    pub struct ListView {
        #[template_child]
        pub listbox: TemplateChild<gtk::ListBox>,
        #[template_child]
        pub search_spinner: TemplateChild<gtk::Box>,
        #[template_child]
        pub earlier: TemplateChild<gtk::Button>,
        #[template_child]
        pub later: TemplateChild<gtk::Button>,
        #[template_child]
        pub earlier_spinner: TemplateChild<gtk::Spinner>,
        #[template_child]
        pub later_spinner: TemplateChild<gtk::Spinner>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ListView {
        const NAME: &'static str = "ListView";
        type Type = super::ListView;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ListView {}
    impl WidgetImpl for ListView {}
    impl BoxImpl for ListView {}
}

glib::wrapper! {
    pub struct ListView(ObjectSubclass<imp::ListView>)
        @extends gtk::Widget, gtk::Box,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Orientable;
}

impl ListView {
    pub fn new() -> Self {
        let list_view: ListView = glib::Object::new::<ListView>(&[]);

        list_view
    }

    pub fn show_spinner(&self) {
        self.clear_list();
        self.imp().earlier.get().set_visible(false);
        self.imp().later.get().set_visible(false);
        self.imp().listbox.get().set_visible(false);
        self.imp().search_spinner.get().set_visible(true);
    }

    pub fn show_all_but_spinner(&self) {
        self.clear_list();
        self.imp().earlier.get().set_visible(true);
        self.imp().later.get().set_visible(true);
        self.imp().search_spinner.get().set_visible(false);
        self.imp().listbox.get().set_visible(true);
        let _ = self.activate_action("win.show-journeys-refresh", None);
    }

    pub fn hide_spinner(&self) {
        self.imp().search_spinner.get().set_visible(false);
    }

    pub fn show_earlier_spinner(&self) {
        self.imp().earlier_spinner.get().set_visible(true);
        self.imp().earlier.get().set_visible(false);
    }

    pub fn hide_earlier_spinner(&self) {
        self.imp().earlier_spinner.get().set_visible(false);
        self.imp().earlier.get().set_visible(true);
    }

    pub fn show_later_spinner(&self) {
        self.imp().later_spinner.get().set_visible(true);
        self.imp().later.get().set_visible(false);
    }

    pub fn hide_later_spinner(&self) {
        self.imp().later_spinner.get().set_visible(false);
        self.imp().later.get().set_visible(true);
    }

    pub fn show_toast(&self, text: &str) {
        let _ = self.activate_action("win.add-toast", Some(&gettext(text).to_variant()));
    }

    pub fn clear_list(&self) {
        // TODO: move to utils and use to clear location list as well
        loop {
            let row = match self.imp().listbox.get().row_at_index(0) {
                Some(r) => r,
                None => break,
            };
            self.imp().listbox.get().remove(&row);
        }
    }
}

impl std::default::Default for ListView {
    fn default() -> Self {
        let list_view: ListView = glib::Object::new::<ListView>(&[]);

        list_view
    }
}
