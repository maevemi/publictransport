use crate::models::client::HafasClient;
use crate::models::location_history::NewLocationHistory;
use crate::models::location_id::LocationId;
use crate::models::SETTINGS;
use crate::widgets::locationsearch::LocationPopoverRow;
use gettextrs::gettext;
use glib::clone;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};
use hafas_client::{BoxError, Location, LocationType};
use log::{error, warn};

mod imp {
    use super::*;
    use crate::models::client::HafasClient;
    use glib::{ParamSpec, ParamSpecObject, Value};
    use once_cell::sync::Lazy;
    use std::cell::RefCell;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/maevemi/publictransport/locationsearch_popover.ui")]
    pub struct LocationPopover {
        // Template widgets
        #[template_child]
        pub station_search_spinner: TemplateChild<gtk::Box>,
        #[template_child]
        pub no_locations_box: TemplateChild<gtk::Box>,
        #[template_child]
        pub autocomplete_listbox: TemplateChild<gtk::ListBox>,
        #[template_child]
        pub scrolled_window_autocomplete: TemplateChild<gtk::ScrolledWindow>,
        pub client: RefCell<HafasClient>,
        pub selected_location_id: RefCell<LocationId>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for LocationPopover {
        const NAME: &'static str = "PublictransportLocationPopover";
        type Type = super::LocationPopover;
        type ParentType = gtk::Popover;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for LocationPopover {
        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecObject::builder::<LocationId>("selected-location-id").build(),
                    ParamSpecObject::builder::<HafasClient>("client").build(),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "client" => {
                    let client = value
                        .get()
                        .expect("The value needs to be of type `HafasClient`.");
                    self.client.replace(client);
                }
                "selected-location-id" => {
                    let selected_location_id = value
                        .get()
                        .expect("The value needs to be of type `String`.");
                    self.selected_location_id.replace(selected_location_id);
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "client" => self.client.borrow().to_value(),
                "selected-location-id" => self.selected_location_id.borrow().to_value(),
                _ => unimplemented!(),
            }
        }

        fn constructed(&self) {
            let obj = self.obj();
            self.parent_constructed();

            obj.setup_signal();
        }
    }
    impl WidgetImpl for LocationPopover {}
    impl PopoverImpl for LocationPopover {}
}

glib::wrapper! {
    pub struct LocationPopover(ObjectSubclass<imp::LocationPopover>)
    @extends gtk::Widget, gtk::Popover,
    @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Native, gtk::ShortcutManager;
}

impl LocationPopover {
    pub fn new(parent: &impl IsA<gtk::Widget>) -> Self {
        let location_popover: LocationPopover = glib::Object::new::<LocationPopover>(&[]);
        location_popover.set_parent(parent);

        location_popover
    }

    pub fn history_locations(&self, location_search_text_str: &String) {
        self.show_search_spinner();

        let hafas_client = &*self.imp().client.borrow();
        let client = hafas_client.get();

        let search_for_adresses_poi: bool = self
            .parent()
            .unwrap()
            .parent()
            .unwrap()
            .property("search-for-adresses-poi");
        let location_type = match search_for_adresses_poi {
            true => LocationType::All,
            false => LocationType::Station,
        };

        let mut locations = match NewLocationHistory::locations(
            location_search_text_str.clone(),
            location_type.clone(),
            client.clone().endpoint.name,
            true,
        ) {
            Ok(l) => l,
            Err(e) => {
                log::error!("Database Error looking up favorites: {}", e);
                let _ = self.activate_action(
                    "win.add-toast",
                    Some(&gettext("Database Error looking up favorites").to_variant()),
                );
                Vec::new()
            }
        };
        let mut history_locations = match NewLocationHistory::locations(
            location_search_text_str.clone(),
            location_type,
            client.endpoint.name,
            false,
        ) {
            Ok(l) => l,
            Err(e) => {
                log::error!("Database Error looking up location history: {}", e);
                let _ = self.activate_action(
                    "win.add-toast",
                    Some(&gettext("Database Error looking up location history").to_variant()),
                );
                Vec::new()
            }
        };
        locations.append(&mut history_locations);

        self.clear_location_list();
        let hafas_client = self.property::<HafasClient>("client");
        for location in locations.iter() {
            let row = LocationPopoverRow::new(location.clone(), &location.name, &hafas_client);
            self.imp().autocomplete_listbox.get().append(&row);
        }

        if locations.len() == 0 {
            self.popdown();
        } else {
            self.show_locations();
            self.popup();
        }
    }

    pub fn locations(&self, search_string: String) {
        self.show_search_spinner();

        let hafas_client = &*self.imp().client.borrow();
        let client = hafas_client.get();

        let search_for_adresses_poi: bool = self
            .parent()
            .unwrap()
            .parent()
            .unwrap()
            .property("search-for-adresses-poi");
        match search_for_adresses_poi {
            true => client.set_locations_type(LocationType::All),
            false => client.set_locations_type(LocationType::Station),
        };

        let locations_handle: tokio::task::JoinHandle<Result<Vec<Location>, BoxError>> =
            crate::RUNTIME.spawn(async move {
                let locations = client.get_locations(search_string).await;
                // std::thread::sleep(std::time::Duration::from_secs(2));
                locations
            });

        gtk_macros::spawn!(clone!(
        @weak self as popover
        => async move {
            match locations_handle.await {
                Ok(locations_handle_inner) => match locations_handle_inner {
                    Ok(locations) => {
                        popover.clear_location_list();
                        let hafas_client = popover.property::<HafasClient>("client");
                        for location in locations.iter() {
                            let row = LocationPopoverRow::new(location.clone(), &location.name, &hafas_client);
                            popover.imp().autocomplete_listbox.get().append(&row);
                        }
                        popover.show_locations();
                    },
                    Err(e) => {
                        popover.show_error();
                        warn!("Error getting locations: {:#?}", e);
                    },
                },
                Err(e) => {
                    popover.show_error();
                    error!("Handle error: {:#?}", e);
                },
            };
        }));
    }

    pub fn setup_signal(&self) {
        self.imp().autocomplete_listbox.get().connect_row_selected(clone!(@weak self as popover => move |_ , row_selected| {
                match row_selected {
                    Some(r) => {
                        popover.parent().unwrap().parent().unwrap().set_property("location-id-set", true);
                        let location_id_row = r.child().unwrap().property::<LocationId>("location-id-row");
                        let location_id = LocationId::new(location_id_row.get());
                        if let Some(location) = location_id.get() {
                            if SETTINGS.get::<bool>("record-history") {
                                let client = popover.property::<HafasClient>("client").get();
                                if let Err(e) = NewLocationHistory::add_location(&location, &client.endpoint.name, false) {
                                    let _ = popover.activate_action(
                                        "win.add-toast",
                                        Some(&gettext("Database Error. Couldn't add history location").to_variant()),
                                    );
                                    log::error!("Database Error. Couldn't add history location: {}", e);
                                };
                            }
                        }
                        popover.set_property("selected-location-id", location_id);
                        popover.parent().unwrap().set_property("text", &r.child().unwrap().property::<String>("location-name-row"));                 
                        popover.parent().unwrap().parent().unwrap().set_property("location-id-set", true);
                    },
                    None => (),
                }
                popover.popdown();
            }));
    }

    pub fn show_search_spinner(&self) {
        self.imp().station_search_spinner.get().set_visible(true);
        self.clear_location_list();
        self.imp().no_locations_box.get().set_visible(false);
        self.imp()
            .scrolled_window_autocomplete
            .get()
            .set_visible(false);
        self.popup();
    }

    pub fn show_locations(&self) {
        self.imp().station_search_spinner.get().set_visible(false);
        self.imp().no_locations_box.set_visible(false);
        self.imp()
            .scrolled_window_autocomplete
            .get()
            .set_visible(true);
    }

    pub fn show_error(&self) {
        self.imp().station_search_spinner.get().set_visible(false);
        self.imp().no_locations_box.set_visible(true);
        self.imp()
            .scrolled_window_autocomplete
            .get()
            .set_visible(false);
    }

    pub fn clear_location_list(&self) {
        loop {
            let row = match self.imp().autocomplete_listbox.get().row_at_index(0) {
                Some(r) => r,
                None => break,
            };
            self.imp().autocomplete_listbox.get().remove(&row);
        }
    }
}

impl std::default::Default for LocationPopover {
    fn default() -> Self {
        glib::Object::new::<Self>(&[])
    }
}
