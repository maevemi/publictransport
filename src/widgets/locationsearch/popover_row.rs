use crate::models::client::HafasClient;
use crate::models::location_history::{LocationHistory, NewLocationHistory};
use crate::models::location_id::LocationId;
use gettextrs::gettext;
use glib::{ParamSpec, ParamSpecString, Value};
use gtk::glib;
use gtk::glib::clone;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use hafas_client::Location;
use once_cell::sync::Lazy;
use std::cell::RefCell;

mod imp {
    use super::*;
    use crate::models::location_id::LocationId;
    use glib::ParamSpecObject;

    #[derive(Debug, Default)]
    pub struct LocationPopoverRow {
        pub location_id: RefCell<LocationId>,
        pub location_name: RefCell<String>,
        pub client: RefCell<HafasClient>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for LocationPopoverRow {
        const NAME: &'static str = "LocationPopoverRow";
        type Type = super::LocationPopoverRow;
        type ParentType = gtk::Box;
    }

    impl ObjectImpl for LocationPopoverRow {
        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecObject::builder::<LocationId>("location-id-row").build(),
                    ParamSpecString::builder("location-name-row").build(),
                    ParamSpecObject::builder::<HafasClient>("client").build(),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "location-id-row" => {
                    let location_id = value
                        .get()
                        .expect("The value needs to be of type `LocationId`.");
                    self.location_id.replace(location_id);
                }
                "location-name-row" => {
                    let location_name = value
                        .get()
                        .expect("The value needs to be of type `String`.");
                    self.location_name.replace(location_name);
                }
                "client" => {
                    let client = value
                        .get()
                        .expect("The value needs to be of type `HafasClient`.");
                    self.client.replace(client);
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "location-id-row" => self.location_id.borrow().to_value(),
                "location-name-row" => self.location_name.borrow().to_value(),
                "client" => self.client.borrow().to_value(),
                _ => unimplemented!(),
            }
        }

        fn constructed(&self) {
            self.parent_constructed();

            // obj.new_location_row();
        }
    }
    impl WidgetImpl for LocationPopoverRow {}
    impl BoxImpl for LocationPopoverRow {}
}

glib::wrapper! {
    pub struct LocationPopoverRow(ObjectSubclass<imp::LocationPopoverRow>)
        @extends gtk::Widget, gtk::Box,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Orientable;
}

impl LocationPopoverRow {
    pub fn new(location_id: Location, location_name: &String, h_client: &HafasClient) -> Self {
        let row: LocationPopoverRow = glib::Object::new::<LocationPopoverRow>(&[
            ("location-id-row", &LocationId::new(Some(location_id))),
            ("location-name-row", location_name),
            ("client", h_client),
        ]);
        row.new_location_row(); // not done in constructed because property location-id-row isn't set then
        row
    }

    pub fn new_location_row(&self) -> &Self {
        self.set_can_focus(false);
        self.set_visible(true);
        self.set_vexpand(true);
        let label = gtk::Label::new(Some(&self.property::<String>("location-name-row")));
        label.set_hexpand(true);
        label.set_xalign(0.0);
        label.set_margin_end(5);
        self.append(&label);

        let favorite_button = gtk::Button::from_icon_name("favorite-symbolic");
        favorite_button.set_has_frame(false);
        favorite_button.add_css_class("circular");
        self.append(&favorite_button);

        let non_favorite_button = gtk::Button::from_icon_name("non-favorite-symbolic");
        non_favorite_button.set_has_frame(false);
        non_favorite_button.add_css_class("circular");
        self.append(&non_favorite_button);

        let location = self
            .property::<LocationId>("location-id-row")
            .get()
            .unwrap();
        let endpoint_name = self.property::<HafasClient>("client").get().endpoint.name;
        favorite_button.connect_clicked(clone!(
            @strong non_favorite_button,
            @strong location,
            @strong endpoint_name,
            @strong self as popover_row => move |button| {
            if let Err(e) = LocationHistory::delete_location(&location, &endpoint_name, true) {
                let _ = popover_row.activate_action(
                    "win.add-toast",
                    Some(&gettext("Database Error. Couldn't remove favorite").to_variant()),
                );
                log::error!("Database Error. Couldn't remove favorite: {}", e);
            } else {
                button.hide();
                non_favorite_button.show();
            }
        }));
        non_favorite_button.connect_clicked(clone!(
            @strong favorite_button,
            @strong location,
            @strong endpoint_name,
            @strong self as popover_row => move |button| {
            if let Err(e) = NewLocationHistory::add_location(&location, &endpoint_name, true) {
                let _ = popover_row.activate_action(
                    "win.add-toast",
                    Some(&gettext("Database Error. Couldn't save favorite").to_variant()),
                );
                log::error!("Database Error. Couldn't save favorite: {}", e);
            } else {
            button.hide();
            favorite_button.show();
            };
        }));

        match LocationHistory::is_favorite(&location) {
            Ok(f) => {
                if f {
                    favorite_button.show();
                    non_favorite_button.hide();
                } else {
                    non_favorite_button.show();
                    favorite_button.hide();
                }
            }
            Err(e) => {
                non_favorite_button.show();
                favorite_button.hide();
                log::error!("Database Error looking up if favorite: {}", e);
                let _ = self.activate_action(
                    "win.add-toast",
                    Some(&gettext("Database Error. Could not look up if favorite").to_variant()),
                );
            }
        };

        self
    }
}
