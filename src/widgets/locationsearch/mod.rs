pub mod popover;
pub mod popover_row;

use self::popover_row::LocationPopoverRow;
use crate::models::location_id::LocationId;

use glib::clone;
use glib::value::Value;
use glib::{ParamSpec, ParamSpecBoolean, ParamSpecString};
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};
use once_cell::sync::Lazy;
use std::cell::{Cell, RefCell};

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/maevemi/publictransport/locationsearch.ui")]
    pub struct LocationSearch {
        #[template_child]
        pub location_search: TemplateChild<gtk::SearchEntry>,
        pub popover: popover::LocationPopover,
        pub location_id: RefCell<String>,
        pub location_id_set: Cell<bool>,
        pub search_for_adresses_poi: Cell<bool>,
        pub reverse_journey: Cell<bool>,
        pub placeholder_text: RefCell<String>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for LocationSearch {
        const NAME: &'static str = "PublictransportLocationSearch";
        type Type = super::LocationSearch;
        type ParentType = gtk::Box; // SearchEntry is not subclassable, so we put it in a box

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for LocationSearch {
        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecString::builder("placeholder-text").build(),
                    ParamSpecBoolean::builder("location-id-set").build(),
                    ParamSpecBoolean::builder("search-for-adresses-poi").build(),
                    ParamSpecBoolean::builder("reverse-journey").build(),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "placeholder-text" => {
                    let input_placeholder_text = value
                        .get()
                        .expect("The value needs to be of type `String`.");
                    self.placeholder_text.replace(input_placeholder_text);
                }
                "location-id-set" => {
                    let location_id_set = value
                        .get()
                        .expect("The value needs to be of type `Boolean`.");
                    self.location_id_set.replace(location_id_set);
                }
                "search-for-adresses-poi" => {
                    let search_for_adresses_poi = value
                        .get()
                        .expect("The value needs to be of type `Boolean`.");
                    self.search_for_adresses_poi
                        .replace(search_for_adresses_poi);
                }
                "reverse-journey" => {
                    let reverse_journey = value
                        .get()
                        .expect("The value needs to be of type `Boolean`.");
                    self.reverse_journey.replace(reverse_journey);
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "placeholder-text" => self.placeholder_text.borrow().to_value(),
                "location-id-set" => self.location_id_set.get().to_value(),
                "search-for-adresses-poi" => self.search_for_adresses_poi.get().to_value(),
                "reverse-journey" => self.reverse_journey.get().to_value(),
                _ => unimplemented!(),
            }
        }

        fn constructed(&self) {
            let obj = self.obj();
            self.parent_constructed();

            self.reverse_journey.set(false);
            self.location_id_set.set(false);

            obj.bind_property(
                "placeholder-text",
                &obj.imp().location_search.get(),
                "placeholder-text",
            )
            .sync_create()
            .build();

            obj.setup_popover();
        }
    }
    impl WidgetImpl for LocationSearch {}
    impl BoxImpl for LocationSearch {}
}

glib::wrapper! {
    pub struct LocationSearch(ObjectSubclass<imp::LocationSearch>)
        @extends gtk::Widget,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Editable;
}

impl LocationSearch {
    pub fn new() -> Self {
        let location_search: LocationSearch = glib::Object::new::<LocationSearch>(&[]);

        location_search.setup_popover();

        location_search
    }

    pub fn setup_popover(&self) {
        let location_search = self.imp().location_search.get();
        self.imp().popover.set_parent(&location_search);

        location_search.connect_activate(
            clone!(@weak self as location_search => move |location_search_text| {
                let location_search_text_str = location_search_text.text().as_str().to_string();

                if &location_search_text_str.len() == &0
                && location_search.imp().reverse_journey.get() == false
                && location_search.imp().location_id_set.get() == false {
                    location_search.imp().popover.set_property("selected-location-id", LocationId::new(None));
                    location_search.imp().popover.history_locations(&location_search_text_str);
                }
            }));

        // Since the connect_move_focus function does not execute if the property "has-focus" changes (seems to be a bug),
        // we work around that connecting to state_flag changes for now
        location_search.connect_state_flags_changed(clone!(@strong location_search, @strong self as loc_search => move |_ , state_flags| {
                if &location_search.text().as_str().to_string().len() == &0
                && loc_search.imp().popover.is_visible() == false
                && loc_search.imp().location_id_set.get() == false {
                    let state_flag_prelight_active = gtk::StateFlags::from_bits(3).unwrap(); // PRELIGHT and ACTIVE stateflag is used to identify focus if mouse is used
                    if state_flags.contains(state_flag_prelight_active) {
                        loc_search.imp().popover.set_property("selected-location-id", LocationId::new(None));
                        loc_search.imp().popover.history_locations(&"".to_string());
                    }
                }

                // Since connecting to move_focus does not work we work around checking the has-focus property
                if loc_search.imp().popover.is_visible() {
                    let text_entry_object = location_search.observe_children().item(1).unwrap();
                    if text_entry_object.property_value("has-focus").get::<bool>().unwrap() == false {
                        loc_search.imp().popover.popdown();
                    }
                }
            }));

        location_search.connect_search_changed(
            clone!(@weak self as location_search => move |location_search_text| {
                let location_search_text_str = location_search_text.text().as_str().to_string();

                if &location_search_text_str.len() == &0
                && location_search.imp().reverse_journey.get() == false
                && location_search.imp().location_id_set.get() == false {
                    location_search.imp().popover.set_property("selected-location-id", LocationId::new(None));
                    location_search.imp().popover.history_locations(&location_search_text_str);
                } else {
                    location_search.imp().reverse_journey.set(false);
                }

                if &location_search_text_str.len() < &3 && location_search.imp().location_id_set.get() == false {
                    location_search.imp().popover.history_locations(&location_search_text_str);
                }

                if &location_search_text_str.len() > &2 && location_search.imp().location_id_set.get() == false {
                    location_search.imp().popover.locations(location_search_text_str.clone());
                } else if &location_search_text_str.len() > &2 && location_search.imp().location_id_set.get() {
                    location_search.imp().popover.popdown();
                    location_search.imp().location_id_set.set(false);
                }
            }),
        );
    }

    pub fn set_text(&self, text: String) {
        if &text.len() > &2 {
            self.imp().location_id_set.set(true);
        }

        self.imp().reverse_journey.set(true);
        self.imp().location_search.set_text(&text);
    }

    pub fn text(&self) -> String {
        self.imp().location_search.text().to_string()
    }
}
