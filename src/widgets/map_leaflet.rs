use glib::clone;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::StringObject;
use gtk::{glib, CompositeTemplate};

use crate::widgets::map_view::MapView;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/maevemi/publictransport/map_leaflet.ui")]
    pub struct MapLeaflet {
        #[template_child]
        pub title: TemplateChild<gtk::Label>,
        #[template_child]
        pub map_view: TemplateChild<MapView>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for MapLeaflet {
        const NAME: &'static str = "MapLeaflet";
        type Type = super::MapLeaflet;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for MapLeaflet {
        fn constructed(&self) {
            let obj = self.obj();
            self.parent_constructed();

            self.map_view.get().connect_notify_local(
                Some("title"),
                clone!(@strong obj => move |window, _| {
                    let title = window.property::<Option<StringObject>>("title");
                    match title {
                        Some(t) => obj.imp().title.set_markup(t.string().as_str()),
                        None => (),
                    }
                }),
            );
        }
    }

    impl WidgetImpl for MapLeaflet {}
    impl BoxImpl for MapLeaflet {}
}

glib::wrapper! {
    pub struct MapLeaflet(ObjectSubclass<imp::MapLeaflet>)
    @extends gtk::Widget, gtk::Box,
    @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Orientable;
}

impl MapLeaflet {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[])
    }
}
