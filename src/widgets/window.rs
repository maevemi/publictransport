use gtk::glib::FromVariant;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{gio, glib, CompositeTemplate};

use crate::models;
use crate::models::{client::HafasClient, datetime::DatetimeModel, journey::HafasJourney};
use crate::widgets::{JourneysMainView, StationboardMainView};

mod imp {
    use super::*;
    use adw::subclass::application_window::AdwApplicationWindowImpl;
    use gtk::glib::{ParamSpec, ParamSpecObject, Value};
    use once_cell::sync::Lazy;
    use std::cell::RefCell;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/maevemi/publictransport/window.ui")]
    pub struct PublictransportWindow {
        #[template_child]
        pub main_view: TemplateChild<JourneysMainView>,
        #[template_child]
        pub stationboard_view: TemplateChild<StationboardMainView>,
        #[template_child]
        pub toast: TemplateChild<adw::ToastOverlay>,
        #[template_child]
        pub leaflet: TemplateChild<adw::Leaflet>,
        #[template_child]
        pub journey_leaflet: TemplateChild<adw::Leaflet>,
        #[template_child]
        pub details_view: TemplateChild<crate::widgets::journeys::detailsview::JourneysDetailsView>,
        #[template_child]
        pub datetime_view: TemplateChild<crate::widgets::datetime::view::DateTimeView>,
        #[template_child]
        pub map_leaflet: TemplateChild<crate::widgets::map_leaflet::MapLeaflet>,
        pub client: RefCell<HafasClient>,
        pub details_journey: RefCell<HafasJourney>,
        pub datetime: RefCell<DatetimeModel>,
    }

    #[gtk::glib::object_subclass]
    impl ObjectSubclass for PublictransportWindow {
        const NAME: &'static str = "PublictransportWindow";
        type Type = super::PublictransportWindow;
        type ParentType = adw::ApplicationWindow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);

            klass.install_action("win.add-toast", Some("s"), |obj, _, message| {
                if let Some(message) = message.and_then(String::from_variant) {
                    let toast = adw::Toast::new(&message);
                    let toast_overlay = obj.imp().toast.get();
                    toast_overlay.add_toast(&toast);
                }
            });

            klass.install_action("win.show-leaflet-page", Some("s"), |obj, _, message| {
                if let Some(message) = message.and_then(String::from_variant) {
                    obj.imp().leaflet.get().set_visible_child_name(&message);
                }
            });

            klass.install_action("win.show-main-leaflet", None, |obj, _, _| {
                obj.imp().leaflet.get().set_visible_child_name("main");
            });

            klass.install_action("win.show-journey-leaflet", None, |obj, _, _| {
                obj.imp()
                    .journey_leaflet
                    .get()
                    .set_visible_child_name("main");
            });

            klass.install_action("win.refresh-details-journey", None, |obj, _, _| {
                let client = obj.imp().client.borrow().get();
                let journey = obj.imp().details_journey.borrow().get();
                obj.imp()
                    .details_view
                    .get()
                    .refresh_journey(&client, &journey.unwrap());
            });
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for PublictransportWindow {
        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecObject::builder::<HafasClient>("client").build(),
                    ParamSpecObject::builder::<HafasJourney>("details-journey").build(),
                    ParamSpecObject::builder::<DatetimeModel>("datetime").build(),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "client" => {
                    let client = value
                        .get()
                        .expect("The value needs to be of type `HafasClient`.");
                    self.client.replace(client);
                }
                "details-journey" => {
                    let details_journey = value
                        .get()
                        .expect("The value needs to be of type `HafasJourney`.");
                    self.details_journey.replace(details_journey);
                }
                "datetime" => {
                    let datetime = value
                        .get()
                        .expect("The value needs to be of type `DateTimeModel`.");
                    self.datetime.replace(datetime);
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "client" => self.client.borrow().to_value(),
                "details-journey" => self.details_journey.borrow().to_value(),
                "datetime" => self.datetime.borrow().to_value(),
                _ => unimplemented!(),
            }
        }

        fn constructed(&self) {
            let obj = self.obj();
            self.parent_constructed();

            obj.bind_property("client", &obj.imp().main_view.get(), "client")
                .bidirectional()
                .build();

            obj.bind_property("client", &obj.imp().stationboard_view.get(), "client")
                .bidirectional()
                .build();

            obj.bind_property(
                "client",
                &obj.imp()
                    .main_view
                    .get()
                    .imp()
                    .departure_place
                    .get()
                    .imp()
                    .popover,
                "client",
            )
            .build();

            obj.bind_property(
                "client",
                &obj.imp()
                    .main_view
                    .get()
                    .imp()
                    .arrival_place
                    .get()
                    .imp()
                    .popover,
                "client",
            )
            .build();

            obj.bind_property(
                "datetime",
                &obj.imp().main_view.get().imp().date_time.get(),
                "datetime",
            )
            .bidirectional()
            .build();

            obj.bind_property(
                "datetime",
                &obj.imp().stationboard_view.get().imp().date_time.get(),
                "datetime",
            )
            .bidirectional()
            .build();

            obj.bind_property("datetime", &obj.imp().datetime_view.get(), "datetime")
                .bidirectional()
                .build();

            obj.connect_notify_local(Some("datetime"), move |window, _| {
                let datetime = window.property::<models::datetime::DatetimeModel>("datetime");
                let client = window.property::<HafasClient>("client").get();
                match datetime.get().clone() {
                    models::datetime::TimeType::Arrival(dt) => {
                        *client.journeys_config.datetime.lock().unwrap() = dt;
                        *client.journeys_config.datetime_type.lock().unwrap() =
                            hafas_client::TimeType::Arrival;
                        *client.stationboard_config.datetime.lock().unwrap() = dt;
                        *client.stationboard_config.datetime_type.lock().unwrap() =
                            hafas_client::TimeType::Arrival;
                    }
                    models::datetime::TimeType::Departure(dt) => {
                        *client.journeys_config.datetime.lock().unwrap() = dt;
                        *client.journeys_config.datetime_type.lock().unwrap() =
                            hafas_client::TimeType::Departure;
                        *client.stationboard_config.datetime.lock().unwrap() = dt;
                        *client.stationboard_config.datetime_type.lock().unwrap() =
                            hafas_client::TimeType::Departure;
                    }
                };
                window.set_property("client", HafasClient::new(&client));
            });

            obj.imp()
                .main_view
                .imp()
                .journeys_view
                .bind_property("details-journey", &*obj, "details-journey")
                .build();

            obj.imp()
                .stationboard_view
                .imp()
                .stationboard_view
                .bind_property(
                    "location-marker",
                    &obj.imp().map_leaflet.imp().map_view.get(),
                    "location-marker",
                )
                .build();

            obj.bind_property(
                "details-journey",
                &obj.imp().map_leaflet.imp().map_view.get(),
                "details-journey",
            )
            .build();

            obj.connect_notify_local(Some("details-journey"), move |window, _| {
                let hafas_journey = &*window.imp().details_journey.borrow();
                let journey = hafas_journey.get().unwrap();
                window.imp().details_view.set_journey_details(&journey);
                window
                    .imp()
                    .journey_leaflet
                    .get()
                    .set_visible_child_name("journey_details");
            });
        }
    }
    impl WidgetImpl for PublictransportWindow {}
    impl WindowImpl for PublictransportWindow {}
    impl ApplicationWindowImpl for PublictransportWindow {}
    impl AdwApplicationWindowImpl for PublictransportWindow {}
}

glib::wrapper! {
    pub struct PublictransportWindow(ObjectSubclass<imp::PublictransportWindow>)
    @extends gtk::Widget, gtk::Window, adw::ApplicationWindow, gtk::ApplicationWindow,
    @implements gio::ActionGroup, gio::ActionMap;
}

impl PublictransportWindow {
    pub fn new<P: glib::IsA<gtk::Application>>(application: &P) -> Self {
        let window: PublictransportWindow =
            glib::Object::new::<Self>(&[("application", application)]);

        // set up shortcuts
        let builder =
            gtk::Builder::from_resource("/com/gitlab/maevemi/publictransport/shortcuts.ui");
        gtk_macros::get_widget!(builder, gtk::ShortcutsWindow, help_overlay);
        window.set_help_overlay(Some(&help_overlay));

        window
    }
}
