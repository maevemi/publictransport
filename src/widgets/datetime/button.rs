use crate::models::datetime::DatetimeModel;
use crate::models::datetime::TimeType::{Arrival, Departure};
use gettextrs::gettext;
use glib::{Object, Value};
use glib::{ParamSpec, ParamSpecObject};
use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use once_cell::sync::Lazy;
use std::cell::RefCell;

mod imp {
    use super::*;

    #[derive(Default)]
    pub struct DatetimeButton {
        pub datetime: RefCell<DatetimeModel>,
    }

    // The central trait for subclassing a GObject
    #[glib::object_subclass]
    impl ObjectSubclass for DatetimeButton {
        const NAME: &'static str = "DatetimeButton";
        type Type = super::DatetimeButton;
        type ParentType = gtk::Button;
    }

    // Trait shared by all GObjects
    impl ObjectImpl for DatetimeButton {
        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> =
                Lazy::new(|| vec![ParamSpecObject::builder::<DatetimeModel>("datetime").build()]);
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "datetime" => {
                    let datetime = value
                        .get()
                        .expect("The value needs to be of type `DatetimeModel`.");
                    self.datetime.replace(datetime);
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "datetime" => self.datetime.borrow().to_value(),
                _ => unimplemented!(),
            }
        }

        fn constructed(&self) {
            let obj = self.obj();
            self.parent_constructed();

            obj.connect_notify_local(Some("datetime"), move |datetime_button, _| {
                let datetime = datetime_button.property::<DatetimeModel>("datetime");
                match datetime.get().clone() {
                    Arrival(Some(dt)) => {
                        datetime_button.set_label(&dt.format("%d.%m.%Y, %H:%M").to_string())
                    }
                    Departure(Some(dt)) => {
                        datetime_button.set_label(&dt.format("%d.%m.%Y, %H:%M").to_string())
                    }
                    _ => datetime_button.set_label(&gettext("Now".to_string())),
                };
            });
        }
    }

    // Trait shared by all widgets
    impl WidgetImpl for DatetimeButton {}

    // Trait shared by all buttons
    impl ButtonImpl for DatetimeButton {
        fn clicked(&self) {
            let button = self.obj();

            let _ =
                button.activate_action("win.show-leaflet-page", Some(&"date_time".to_variant()));
        }
    }
}

glib::wrapper! {
    pub struct DatetimeButton(ObjectSubclass<imp::DatetimeButton>)
    @extends gtk::Button, gtk::Widget,
    @implements gtk::Accessible, gtk::Actionable, gtk::Buildable, gtk::ConstraintTarget;
}

impl DatetimeButton {
    pub fn new() -> Self {
        Object::new::<Self>(&[])
    }

    pub fn with_label(label: &str) -> Self {
        Object::new::<Self>(&[("label", &label)])
    }
}

impl std::default::Default for DatetimeButton {
    fn default() -> Self {
        glib::Object::new::<Self>(&[])
    }
}
