use crate::models::datetime::DatetimeModel;
use crate::models::datetime::TimeType::{Arrival, Departure};
use chrono::prelude::*;
use glib::clone;
use glib::value::Value;
use glib::ParamSpec;
use glib::ParamSpecObject;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};
use once_cell::sync::Lazy;
use std::cell::RefCell;

mod imp {
    use super::*;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/maevemi/publictransport/datetime_view.ui")]
    pub struct DateTimeView {
        #[template_child]
        pub time_hh: TemplateChild<gtk::SpinButton>,
        #[template_child]
        pub time_mm: TemplateChild<gtk::SpinButton>,
        #[template_child]
        pub calendar: TemplateChild<gtk::Calendar>,
        #[template_child]
        pub check_departure: TemplateChild<gtk::CheckButton>,
        #[template_child]
        pub check_arrival: TemplateChild<gtk::CheckButton>,
        #[template_child]
        pub ok_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub now_button: TemplateChild<gtk::Button>,
        pub datetime: RefCell<DatetimeModel>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for DateTimeView {
        const NAME: &'static str = "DateTimeView";
        type Type = super::DateTimeView;
        type ParentType = gtk::Box; // SearchEntry is not subclassable, so we put it in a box

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for DateTimeView {
        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> =
                Lazy::new(|| vec![ParamSpecObject::builder::<DatetimeModel>("datetime").build()]);
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "datetime" => {
                    let datetime = value
                        .get()
                        .expect("The value needs to be of type `HafasClient`.");
                    self.datetime.replace(datetime);
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "datetime" => self.datetime.borrow().to_value(),
                _ => unimplemented!(),
            }
        }

        fn constructed(&self) {
            let obj = self.obj();
            self.parent_constructed();

            obj.setup_datetime_view();

            obj.connect_notify_local(Some("datetime"), move |datetime_view, _| {
                datetime_view.setup_datetime_view();
            });

            self.ok_button.connect_clicked(clone!(
                @strong obj => move |_| {
                let year = obj.imp().calendar.year() as i32;
                let month = obj.imp().calendar.month() as u32;
                let day = obj.imp().calendar.day() as u32;
                let dt = Local.with_ymd_and_hms(year, month+1, day, obj.imp().time_hh.value_as_int() as u32,
                    obj.imp().time_mm.value_as_int() as u32, 0).unwrap();

                let mut datetime = Arrival(Some(dt));
                if obj.imp().check_departure.is_active() == true {
                    datetime = Departure(Some(dt));
                }

                let datetime_model = DatetimeModel::new(datetime);
                obj.set_property("datetime", datetime_model);

                let _ = obj.activate_action("win.show-main-leaflet", None);
            }));

            self.now_button.connect_clicked(clone!(
                @strong obj => move |_| {
                let mut datetime = Arrival(None);
                if obj.imp().check_departure.is_active() == true {
                    datetime = Departure(None)
                }

                let datetime_model = DatetimeModel::new(datetime);
                obj.set_property("datetime", datetime_model);

                let _ = obj.activate_action("win.show-main-leaflet", None);
            }));
        }
    }
    impl WidgetImpl for DateTimeView {}
    impl BoxImpl for DateTimeView {}
}

glib::wrapper! {
    pub struct DateTimeView(ObjectSubclass<imp::DateTimeView>)
        @extends gtk::Widget,
        @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Editable;
}

impl DateTimeView {
    pub fn new() -> Self {
        let date_time_view: DateTimeView = glib::Object::new::<DateTimeView>(&[]);

        date_time_view.setup_datetime_view();

        date_time_view
    }

    pub fn setup_datetime_view(&self) {
        let datetime_model = self.property::<DatetimeModel>("datetime");

        let datetime_option = match datetime_model.get() {
            Departure(dt) => {
                self.imp().check_departure.set_active(true);
                dt
            }
            Arrival(dt) => {
                self.imp().check_departure.set_active(false);
                dt
            }
        };

        let datetime = match datetime_option {
            Some(dt) => dt,
            None => Local::now(),
        };

        self.imp().calendar.set_property(
            "year",
            datetime.format("%Y").to_string().parse::<i32>().unwrap(),
        );
        self.imp().calendar.set_property(
            "month",
            datetime.format("%m").to_string().parse::<i32>().unwrap() - 1,
        );
        self.imp().calendar.set_property(
            "day",
            datetime.format("%d").to_string().parse::<i32>().unwrap(),
        );
        self.imp()
            .time_hh
            .set_value(datetime.format("%H").to_string().parse::<f64>().unwrap());
        self.imp()
            .time_mm
            .set_value(datetime.format("%M").to_string().parse::<f64>().unwrap());
    }
}

impl std::default::Default for DateTimeView {
    fn default() -> Self {
        glib::Object::new::<Self>(&[])
    }
}
