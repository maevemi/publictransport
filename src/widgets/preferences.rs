use crate::models::{location_history::LocationHistory, SETTINGS};
use adw::{prelude::*, MessageDialog};
use gettextrs::gettext;
use gtk::{
    glib::{self, clone},
    subclass::prelude::*,
    CompositeTemplate, Inhibit,
};

mod imp {
    use std::cell::RefCell;

    use adw::subclass::{preferences_window::PreferencesWindowImpl, window::AdwWindowImpl};
    use glib::subclass::{self};
    use gtk::glib::SignalHandlerId;

    use super::*;

    #[derive(Debug, CompositeTemplate)]
    #[template(resource = "/com/gitlab/maevemi/publictransport/preferences.ui")]
    pub struct PreferencesWindow {
        #[template_child]
        pub show_map_switch: TemplateChild<gtk::Switch>,
        #[template_child]
        pub record_history_switch: TemplateChild<gtk::Switch>,
        #[template_child]
        pub delete_favorites_button: TemplateChild<gtk::Button>,
        pub record_history_switch_handler_id: RefCell<Option<SignalHandlerId>>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for PreferencesWindow {
        const NAME: &'static str = "PreferencesWindow";
        type Type = super::PreferencesWindow;
        type ParentType = adw::PreferencesWindow;

        fn new() -> Self {
            Self {
                show_map_switch: TemplateChild::default(),
                record_history_switch: TemplateChild::default(),
                delete_favorites_button: TemplateChild::default(),
                record_history_switch_handler_id: RefCell::new(None),
            }
        }

        fn class_init(klass: &mut Self::Class) {
            klass.bind_template();
        }

        fn instance_init(obj: &subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for PreferencesWindow {
        fn constructed(&self) {
            self.parent_constructed();
        }
    }

    impl WidgetImpl for PreferencesWindow {}
    impl WindowImpl for PreferencesWindow {}
    impl AdwWindowImpl for PreferencesWindow {}
    impl PreferencesWindowImpl for PreferencesWindow {}
}

glib::wrapper! {
    pub struct PreferencesWindow(ObjectSubclass<imp::PreferencesWindow>)
        @extends gtk::Widget, gtk::Window, adw::Window, adw::PreferencesWindow;
}

impl PreferencesWindow {
    pub fn new() -> Self {
        let window = glib::Object::new::<Self>(&[]);
        window.setup_widgets();
        window
    }

    pub fn setup_widgets(&self) {
        SETTINGS
            .bind("show-map", &*self.imp().show_map_switch, "active")
            .build();
        SETTINGS
            .bind(
                "record-history",
                &*self.imp().record_history_switch,
                "active",
            )
            .build();

        let record_history_switch_handler = self.imp().record_history_switch.connect_notify_local(
            Some("active"),
            clone!(@strong self as preferences_window => move |record_history_switch, _| {
                if record_history_switch.is_active() == false {
                    preferences_window.delete_history_dialog();
                }
            }),
        );
        *self.imp().record_history_switch_handler_id.borrow_mut() =
            Some(record_history_switch_handler);

        self.connect_close_request(|pref_win| {
            if let Some(id) = pref_win.imp().record_history_switch_handler_id.take() {
                pref_win.imp().record_history_switch.disconnect(id);
            }
            Inhibit::default()
        });

        self.imp().delete_favorites_button.connect_clicked(
            clone!(@strong self as pref_win => move |_| {
                pref_win.delete_favorites_dialog();
            }),
        );
    }

    pub fn delete_history_dialog(&self) {
        let delete_dialog = MessageDialog::new(
            Some(self),
            Some(&gettext("Delete Search History?")),
            Some(&gettext(
                "The records of your location searches will be deleted.",
            )),
        );
        delete_dialog.add_responses(&[
            ("cancel", &gettext("Cancel")),
            ("delete", &gettext("Delete")),
        ]);
        delete_dialog.set_response_appearance("delete", adw::ResponseAppearance::Destructive);

        let record_history_switch = self.imp().record_history_switch.get();

        delete_dialog.connect_response(
            Some("delete"),
            clone!(@strong self as pref_win => move |_, _| {
                if let Err(e) = LocationHistory::delete_locations(false) {
                    log::error!("Database Error deleting history: {}", e);
                    let toast = adw::Toast::new(&gettext("Database Error. Maybe your Search history wasn't deleted"));
                    pref_win.add_toast(&toast);
                } else {
                    let toast = adw::Toast::new(&gettext("Search History deleted"));
                    pref_win.add_toast(&toast);
                };
            }),
        );
        delete_dialog.connect_response(
            Some("cancel"),
            clone!(@strong record_history_switch => move |_ , _| {
                record_history_switch.set_active(true);
            }),
        );
        delete_dialog.connect_response(
            Some("close"),
            clone!(@strong record_history_switch => move |_ , _| {
                record_history_switch.set_active(true);
            }),
        );

        delete_dialog.present()
    }

    pub fn delete_favorites_dialog(&self) {
        let delete_dialog = MessageDialog::new(
            Some(self),
            Some(&gettext("Delete Favorites?")),
            Some(&gettext(
                "Are you sure you want to delete all your Favorites?",
            )),
        );
        delete_dialog.add_responses(&[("no", &gettext("No")), ("yes", &gettext("Yes"))]);
        delete_dialog.set_response_appearance("yes", adw::ResponseAppearance::Destructive);

        delete_dialog.connect_response(
            Some("yes"),
            clone!(@strong self as pref_win => move |_, _| {
                if let Err(e) = LocationHistory::delete_locations(true) {
                    log::error!("Database Error deleting favorites: {}", e);
                    let toast = adw::Toast::new(&gettext("Database Error. Maybe your Search favorites aren't deleted"));
                    pref_win.add_toast(&toast);
                } else {
                    let toast = adw::Toast::new(&gettext("Favorites deleted"));
                    pref_win.add_toast(&toast);
                };
            }),
        );

        delete_dialog.present()
    }
}
