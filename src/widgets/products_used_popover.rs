use crate::models::products_used::ProductsUsedModel;
use glib::clone;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};

mod imp {
    use super::*;
    use crate::models::products_used::ProductsUsedModel;
    use glib::{ParamSpec, ParamSpecObject, Value};
    use once_cell::sync::Lazy;
    use std::cell::RefCell;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/maevemi/publictransport/products_used_popover.ui")]
    pub struct ProductsUsed {
        #[template_child]
        pub products_listbox: TemplateChild<gtk::ListBox>,
        pub products_used: RefCell<ProductsUsedModel>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ProductsUsed {
        const NAME: &'static str = "ProductsUsed";
        type Type = super::ProductsUsed;
        type ParentType = gtk::Popover;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for ProductsUsed {
        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![ParamSpecObject::builder::<ProductsUsedModel>("products-used").build()]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "products-used" => {
                    let products_used = value
                        .get()
                        .expect("The value needs to be of type `HafasClient`.");
                    self.products_used.replace(products_used);
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "products-used" => self.products_used.borrow().to_value(),
                _ => unimplemented!(),
            }
        }

        fn constructed(&self) {
            let obj = self.obj();
            self.parent_constructed();
            obj.products_used();
        }
    }

    impl WidgetImpl for ProductsUsed {}
    impl PopoverImpl for ProductsUsed {}
}

glib::wrapper! {
    pub struct ProductsUsed(ObjectSubclass<imp::ProductsUsed>)
    @extends gtk::Widget, gtk::Popover,
    @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Native, gtk::ShortcutManager;
}

impl ProductsUsed {
    pub fn new(parent: &impl IsA<gtk::Widget>, products_used: ProductsUsedModel) -> Self {
        let products_used_popover: ProductsUsed =
            glib::Object::new::<ProductsUsed>(&[("products-used", &products_used)]);

        products_used_popover.set_parent(parent);
        products_used_popover.products_used();

        products_used_popover
    }

    pub fn products_used(&self) {
        let products_used = self.property::<ProductsUsedModel>("products-used").get();

        let mut i = 0;
        for product_used in products_used.iter() {
            let product_used_switch = gtk::Switch::new();
            product_used_switch.set_halign(gtk::Align::End);
            product_used_switch.set_valign(gtk::Align::Center);
            product_used_switch.set_active(*product_used.used.lock().unwrap());

            product_used_switch.connect_state_notify(
                clone!(@weak self as self_clone, @strong products_used => move |switch| {
                    *products_used[i].used.lock().unwrap() = switch.is_active();
                    let products_used_model = ProductsUsedModel::new(&products_used);
                    self_clone.set_property("products-used", products_used_model);
                }),
            );

            let label = gtk::Label::new(Some(&product_used.product.name));
            let dummy_box = gtk::Box::new(gtk::Orientation::Horizontal, 10);
            dummy_box.set_hexpand(true);
            let row = gtk::Box::new(gtk::Orientation::Horizontal, 10);
            row.set_hexpand(true);
            row.set_height_request(35);
            row.append(&label);
            row.append(&dummy_box);
            row.append(&product_used_switch);

            self.imp().products_listbox.get().append(&row);
            i += 1;
        }
    }
}

impl std::default::Default for ProductsUsed {
    fn default() -> Self {
        glib::Object::new::<Self>(&[])
    }
}
