use glib::clone;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};
use hafas_client::{BoxError, StationBoard};
use log::{error, warn};

use crate::models::SETTINGS;
use crate::models::{
    client::HafasClient, location_marker::LocationMarker, stationboard::HafasStationboard,
};
use crate::widgets::map_view::{MapView, MarkerType};
use crate::widgets::stationboard::stationboard_row::StationboardRow;

mod imp {
    use crate::models::location_marker::LocationMarker;

    use super::*;
    use glib::{ParamSpec, ParamSpecObject, Value};
    use once_cell::sync::Lazy;
    use std::cell::RefCell;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/maevemi/publictransport/stationboard_view.ui")]
    pub struct StationboardView {
        #[template_child]
        pub map_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub map_view: TemplateChild<MapView>,
        #[template_child]
        pub list_view: TemplateChild<crate::widgets::list_view::ListView>,
        pub marker: shumate::Marker,
        pub client: RefCell<HafasClient>,
        pub stationboard: RefCell<HafasStationboard>,
        pub location_marker: RefCell<LocationMarker>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for StationboardView {
        const NAME: &'static str = "StationboardView";
        type Type = super::StationboardView;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for StationboardView {
        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecObject::builder::<HafasClient>("client").build(),
                    ParamSpecObject::builder::<HafasStationboard>("stationboard").build(),
                    ParamSpecObject::builder::<LocationMarker>("location-marker").build(),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "client" => {
                    let client = value
                        .get()
                        .expect("The value needs to be of type `HafasClient`.");
                    self.client.replace(client);
                }
                "stationboard" => {
                    let stationboard = value
                        .get()
                        .expect("The value needs to be of type `HafasStationboard`.");
                    self.stationboard.replace(stationboard);
                }
                "location-marker" => {
                    let location_marker = value
                        .get()
                        .expect("The value needs to be of type `HafasStationboard`.");
                    self.location_marker.replace(location_marker);
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "client" => self.client.borrow().to_value(),
                "stationboard" => self.stationboard.borrow().to_value(),
                "location-marker" => self.location_marker.borrow().to_value(),
                _ => unimplemented!(),
            }
        }

        fn constructed(&self) {
            let obj = self.obj();

            obj.setup_signals();

            self.parent_constructed();

            self.list_view
                .imp()
                .listbox
                .connect_row_activated(move |_list, row| {
                    let leg_row = &*row.downcast_ref::<StationboardRow>().unwrap();
                    leg_row.stopovers();
                });

            self.map_button.get().connect_clicked(clone!(
            @strong obj => move |_| {
                let _ = obj.activate_action("win.show-leaflet-page", Some(&"map".to_variant()));
            }));

            SETTINGS.connect_changed(
                Some("show-map"),
                clone!(@weak obj => move |_,_| {
                    obj.stationboards();
                }),
            );
        }
    }
    impl WidgetImpl for StationboardView {}
    impl BoxImpl for StationboardView {}
}

glib::wrapper! {
    pub struct StationboardView(ObjectSubclass<imp::StationboardView>)
    @extends gtk::Widget, gtk::Box,
    @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Orientable;
}

impl StationboardView {
    pub fn new() -> Self {
        let stationboard_view: StationboardView = glib::Object::new::<StationboardView>(&[]);

        stationboard_view
    }

    pub fn setup_signals(&self) {
        self.connect_notify_local(Some("client"), move |stationboard_view, _| {
            let client = stationboard_view.imp().client.borrow().get();

            if client.get_stationboard_id().is_some() {
                stationboard_view.stationboards();
            }
        });

        let list_view = self.imp().list_view.imp();

        list_view.earlier.connect_clicked(clone!(
        @weak self as stationboard_view
        => move |_| {
            stationboard_view.earlier();
        }));

        list_view.later.connect_clicked(clone!(
        @weak self as stationboard_view
        => move |_| {
            stationboard_view.later();
        }));

        list_view.listbox.connect_row_activated(
            //ToDo
            clone!(@weak self as stationboard_view => move |_list, row| {
                let row = row.downcast_ref::<StationboardRow>().unwrap();
                let hafas_stationboard = &*row.imp().stationboard.borrow();
                stationboard_view.set_property("stationboard", hafas_stationboard);
            }),
        );
    }

    pub fn stationboards(&self) {
        self.imp().list_view.show_spinner();
        self.imp().map_button.set_visible(false);

        let hafas_client = &*self.imp().client.borrow();
        let client = hafas_client.get().clone();

        let stationbard_handle: tokio::task::JoinHandle<Result<Vec<StationBoard>, BoxError>> =
            crate::RUNTIME.spawn(async move {
                let stationboards = client.get_stationboard().await;
                // std::thread::sleep(std::time::Duration::from_secs(2));
                stationboards
            });

        gtk_macros::spawn!(clone!(
        @weak self as stationboard_view
        => async move {
            match stationbard_handle.await {
                Ok(stationbard_handle_inner) => match stationbard_handle_inner {
                    Ok(stationboards) => {
                        stationboard_view.imp().list_view.show_all_but_spinner();

                        if SETTINGS.get::<bool>("show-map") {
                            if let Some(place) = &stationboards[0].stop {
                                // check wether search is for Arrivals or Departures (TODO @hafas_crate: store info explictly?)
                                if let Some(stopovers) = &stationboards[0].stopovers {
                                    if &place.id == &stopovers[0].stop.id {
                                        stationboard_view.imp().map_view.show_location(MarkerType::Origin, place);
                                        stationboard_view.set_property("location-marker", LocationMarker::new(place, MarkerType::Origin));
                                    } else {
                                        stationboard_view.imp().map_view.show_location(MarkerType::Destination, place);
                                        stationboard_view.set_property("location-marker", LocationMarker::new(place, MarkerType::Destination));
                                    }
                                }
                                stationboard_view.imp().map_view.imp().map.scale().unwrap().hide();
                                stationboard_view.imp().map_view.imp().map.set_show_zoom_buttons(false);

                                stationboard_view.imp().map_button.set_visible(true);
                            }
                        }

                        let _ = stationboard_view.activate_action("win.show-stationboard-refresh", None);
                        for stationboard in stationboards.iter() {
                            let row = StationboardRow::new(&stationboard);
                            stationboard_view.imp().list_view.imp().listbox.get().append(&row);
                        }
                    },
                    Err(e) => {
                        warn!("Error getting stationboards: {:#?}", e);
                        stationboard_view.imp().list_view.hide_spinner();
                        let _ = stationboard_view.activate_action("win.hide-stationboard-refresh", None);
                        stationboard_view.show_toast("No Stationboards found");
                    },
                },
                Err(e) => {
                    stationboard_view.show_toast("internal Error getting Stationboards");
                    error!("Handle error: {:#?}", e);
                    stationboard_view.imp().list_view.hide_spinner();
                    let _ = stationboard_view.activate_action("win.hide-stationboard-refresh", None);
                },
            };
            // hide earlier/later button until funtionality is implemented
            stationboard_view.imp().list_view.imp().earlier.set_visible(false);
            stationboard_view.imp().list_view.imp().later.set_visible(false);
        }));
    }

    pub fn earlier(&self) {
        // self.imp().list_view.show_earlier_spinner();

        // let hafas_client = &*self.imp().client.borrow();
        // let client = hafas_client.get().clone();

        // let stationbard_handle: tokio::task::JoinHandle<Result<Journeys, BoxError>> = crate::RUNTIME
        //     .spawn(async move {
        //         let journeys = client.get_earlier_journeys().await;
        //         // std::thread::sleep(std::time::Duration::from_secs(2));
        //         journeys
        //     });

        // gtk_macros::spawn!(clone!(
        // @weak self as stationboard_view
        // => async move {
        //     match stationbard_handle.await {
        //         Ok(stationbard_handle_inner) => match stationbard_handle_inner {
        //             Ok(mut journeys) => {
        //                 stationboard_view.imp().list_view.hide_earlier_spinner();
        //                 journeys.journeys.reverse();
        //                 // for stationboard in journeys.journeys.iter() {
        //                 //     let row = StationboardRow::new(&stationboard);
        //                 //     stationboard_view.imp().list_view.imp().listbox.get().prepend(&row);
        //                 // }
        //             },
        //             Err(e) => {
        //                 warn!("Error getting earlier journeys: {:#?}", e);
        //                 stationboard_view.imp().list_view.hide_earlier_spinner();
        //                 stationboard_view.show_toast("Error getting earlier Journeys");
        //             },
        //         },
        //         Err(e) => {
        //             error!("Handle error: {:#?}", e);
        //             stationboard_view.imp().list_view.hide_earlier_spinner();
        //             stationboard_view.show_toast("internal Error getting earlier Journeys");
        //         },
        //     };
        // }));
    }

    pub fn later(&self) {
        // self.imp().list_view.show_later_spinner();

        // let hafas_client = &*self.imp().client.borrow();
        // let client = hafas_client.get().clone();

        // let stationbard_handle: tokio::task::JoinHandle<Result<Journeys, BoxError>> = crate::RUNTIME
        //     .spawn(async move {
        //         let journeys = client.get_later_journeys().await;
        //         // std::thread::sleep(std::time::Duration::from_secs(2));
        //         journeys
        //     });

        // gtk_macros::spawn!(clone!(
        // @weak self as stationboard_view
        // => async move {
        //     match stationbard_handle.await {
        //         Ok(stationbard_handle_inner) => match stationbard_handle_inner {
        //             Ok(stationboard) => {
        //                 stationboard_view.imp().list_view.hide_later_spinner();
        //                 // for stationboard in journeys.journeys.iter() {
        //                 //     let row = StationboardRow::new(&stationboard);
        //                 //     stationboard_view.imp().list_view.imp().listbox.get().append(&row);
        //                 // }
        //             },
        //             Err(e) => {
        //                 warn!("Error getting later journeys: {:#?}", e);
        //                 stationboard_view.imp().list_view.hide_later_spinner();
        //                 stationboard_view.show_toast("Error getting later Journeys");

        //             },
        //         },
        //         Err(e) => {
        //             error!("Handle error: {:#?}", e);
        //             stationboard_view.imp().list_view.hide_later_spinner();
        //             stationboard_view.show_toast("internal Error getting later Journeys");
        //         },
        //     };
        // }));
    }

    pub fn show_toast(&self, text: &str) {
        self.imp().list_view.show_toast(text);
    }
}
