use crate::models::stationboard::HafasStationboard;
use crate::widgets::stopover_row::StopoverRow;
use gettextrs::gettext;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};
use hafas_client::StationBoard;

mod imp {
    use super::*;
    use std::cell::RefCell;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/maevemi/publictransport/stationboards_stationboard_row.ui")]
    pub struct StationboardRow {
        #[template_child]
        pub planned_time: TemplateChild<gtk::Label>,
        #[template_child]
        pub time_delay: TemplateChild<gtk::Label>,
        #[template_child]
        pub stop: TemplateChild<gtk::Label>,
        #[template_child]
        pub track: TemplateChild<gtk::Label>,
        #[template_child]
        pub expand_symbol: TemplateChild<gtk::Image>,
        #[template_child]
        pub minimize_symbol: TemplateChild<gtk::Image>,
        #[template_child]
        pub line: TemplateChild<gtk::Box>,
        #[template_child]
        pub stopover_listbox: TemplateChild<gtk::ListBox>,
        pub stationboard: RefCell<HafasStationboard>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for StationboardRow {
        const NAME: &'static str = "StationboardRow";
        type Type = super::StationboardRow;
        type ParentType = gtk::ListBoxRow;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for StationboardRow {
        fn constructed(&self) {
            self.parent_constructed();
        }
    }
    impl WidgetImpl for StationboardRow {}
    impl ListBoxRowImpl for StationboardRow {}
}

glib::wrapper! {
    pub struct StationboardRow(ObjectSubclass<imp::StationboardRow>)
    @extends gtk::Widget, gtk::ListBoxRow,
    @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Actionable;
}

impl StationboardRow {
    pub fn new(stationboard: &StationBoard) -> Self {
        let stationboard_row: StationboardRow = glib::Object::new::<StationboardRow>(&[]);
        *stationboard_row.imp().stationboard.borrow_mut() =
            HafasStationboard::new(&stationboard.clone());
        stationboard_row.fill();
        stationboard_row
    }

    pub fn fill(&self) -> &Self {
        let stationboard = self.imp().stationboard.borrow().get().unwrap();
        let planned_time = &stationboard.plannedTime.unwrap();

        let time_delay = match &stationboard.time {
            Some(d) => {
                let mut delay = String::from("+");
                delay.push_str(
                    &d.signed_duration_since(*planned_time)
                        .num_minutes()
                        .to_string(),
                );
                delay
            }
            None => String::from(""),
        };

        // TODO create help function (can be used by journeyRow as well)
        let platform = match &stationboard.platform {
            Some(d) => {
                let mut str = gettext("Pl. ").to_string();
                str.push_str(&d);
                str
            }
            None => "".to_string(),
        };

        let destination = stationboard.direction.clone();

        let imp = self.imp();

        let line_box = gtk::Box::new(gtk::Orientation::Horizontal, 0);
        line_box.set_halign(gtk::Align::Center);
        line_box.set_valign(gtk::Align::Center);
        match &stationboard.name {
            Some(l) => {
                let mut line_str = " ".to_string();
                line_str.push_str(&l);
                line_str.push_str(&" ".to_string());

                line_box.append(&gtk::Label::new(Some(&line_str.to_string())));
            }
            None => {
                let image = gtk::Image::new();
                image.set_icon_name(Some("walking-symbolic"));
                image.set_margin_top(3);
                image.set_margin_bottom(3);
                line_box.append(&image);
            }
        }
        let dark = adw::StyleManager::default().is_dark();
        if dark {
            line_box.add_css_class("product-background-dark")
        } else {
            line_box.add_css_class("product-background")
        }
        imp.line.get().append(&line_box);

        if let Some(stopovers) = &stationboard.stopovers {
            if stopovers.len() != 0 {
                imp.expand_symbol.set_visible(true);
            }
        }

        self.imp().track.get().set_markup(&platform);
        self.imp()
            .stop
            .get()
            .set_markup(&destination.unwrap_or("unknown".to_string()).clone());
        self.imp()
            .planned_time
            .get()
            .set_markup(&planned_time.format("%H:%M").to_string());
        self.imp().time_delay.get().set_markup(&time_delay);

        self
    }

    pub fn stopovers(&self) {
        let stationboard = self.imp().stationboard.borrow().get().unwrap();
        if self.imp().stopover_listbox.get().row_at_index(0).is_none()
            && self.imp().stopover_listbox.get().is_visible() == false
        {
            if let Some(stopovers) = stationboard.stopovers {
                if stopovers.len() != 0 {
                    // TODO: adjust hafas-client-rs to retun None if len = 0
                    for stopover in stopovers.iter() {
                        self.imp()
                            .stopover_listbox
                            .get()
                            .append(&StopoverRow::new(stopover));
                    }
                    self.imp().stopover_listbox.set_visible(true);
                    self.imp().expand_symbol.set_visible(false);
                    self.imp().minimize_symbol.set_visible(true);
                }
            }
        } else if self.imp().stopover_listbox.get().row_at_index(0).is_some()
            && self.imp().stopover_listbox.get().is_visible() == false
        {
            self.imp().stopover_listbox.set_visible(true);
            self.imp().expand_symbol.set_visible(false);
            self.imp().minimize_symbol.set_visible(true);
        } else {
            self.imp().stopover_listbox.set_visible(false);
            self.imp().expand_symbol.set_visible(true);
            self.imp().minimize_symbol.set_visible(false);
        }
    }
}
