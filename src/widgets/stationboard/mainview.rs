use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};

use crate::models::products_used::ProductsUsedModel;

mod imp {
    use super::*;
    use crate::models::client::HafasClient;
    use crate::models::location_id::LocationId;
    use glib::{ParamSpec, ParamSpecObject, Value};
    use once_cell::sync::Lazy;
    use std::cell::RefCell;

    #[derive(Debug, Default, CompositeTemplate)]
    #[template(resource = "/com/gitlab/maevemi/publictransport/stationboard_mainview.ui")]
    pub struct StationboardMainView {
        // Template widgets
        #[template_child]
        pub place: TemplateChild<crate::widgets::locationsearch::LocationSearch>,
        #[template_child]
        pub stationboard_view:
            TemplateChild<crate::widgets::stationboard::stationboard_view::StationboardView>,
        #[template_child]
        pub date_time: TemplateChild<crate::widgets::datetime::button::DatetimeButton>,
        #[template_child]
        pub refresh_stationboard: TemplateChild<gtk::Button>,
        #[template_child]
        pub transportation_means: TemplateChild<gtk::Button>,
        #[template_child]
        pub transportation_means_image: TemplateChild<gtk::Image>,
        pub products_used_popover: crate::widgets::products_used_popover::ProductsUsed,
        pub client: RefCell<HafasClient>,
        pub stop_id: RefCell<LocationId>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for StationboardMainView {
        const NAME: &'static str = "StationboardMainView";
        type Type = super::StationboardMainView;
        type ParentType = gtk::Box;

        fn class_init(klass: &mut Self::Class) {
            Self::bind_template(klass);

            klass.install_action("win.show-stationboard-refresh", None, |obj, _, _| {
                obj.imp().refresh_stationboard.get().set_visible(true);
            });

            klass.install_action("win.hide-stationboard-refresh", None, |obj, _, _| {
                obj.imp().refresh_stationboard.get().set_visible(false);
            });
        }

        fn instance_init(obj: &glib::subclass::InitializingObject<Self>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for StationboardMainView {
        fn properties() -> &'static [ParamSpec] {
            static PROPERTIES: Lazy<Vec<ParamSpec>> = Lazy::new(|| {
                vec![
                    ParamSpecObject::builder::<HafasClient>("client").build(),
                    ParamSpecObject::builder::<LocationId>("stop-id").build(),
                ]
            });
            PROPERTIES.as_ref()
        }

        fn set_property(&self, _id: usize, value: &Value, pspec: &ParamSpec) {
            match pspec.name() {
                "client" => {
                    let client = value
                        .get()
                        .expect("The value needs to be of type `HafasClient`.");
                    self.client.replace(client);
                }
                "stop-id" => {
                    let stop_id = value
                        .get()
                        .expect("The value needs to be of type `LocationId`.");
                    self.stop_id.replace(stop_id);
                }
                _ => unimplemented!(),
            }
        }

        fn property(&self, _id: usize, pspec: &ParamSpec) -> Value {
            match pspec.name() {
                "client" => self.client.borrow().to_value(),
                "stop-id" => self.stop_id.borrow().to_value(),
                _ => unimplemented!(),
            }
        }

        fn constructed(&self) {
            self.parent_constructed();
            let obj = self.obj();

            obj.bind_property("client", &obj.imp().stationboard_view.get(), "client")
                .build();

            obj.bind_property("client", &obj.imp().products_used_popover, "products-used")
                .transform_to(|_, value: HafasClient| {
                    let client = value.get();
                    let products_used =
                        ProductsUsedModel::new(&client.stationboard_config.products_used).get();
                    Some(ProductsUsedModel::new(&products_used).to_value())
                })
                .transform_from(|binding, value: ProductsUsedModel| {
                    let products_used = value.get();
                    let client = binding
                        .source()
                        .unwrap()
                        .property::<HafasClient>("client")
                        .get();
                    for (i, product_used) in products_used.iter().enumerate() {
                        *client.stationboard_config.products_used[i]
                            .used
                            .lock()
                            .unwrap() = *product_used.used.lock().unwrap();
                    }
                    Some(HafasClient::new(&client).to_value())
                })
                .bidirectional()
                .build();

            obj.imp()
                .place
                .get()
                .imp()
                .popover
                .bind_property("selected-location-id", &*obj, "stop-id")
                .build();

            obj.connect_notify_local(Some("stop-id"), move |stationboard_main_view, _| {
                let stop_id_option = stationboard_main_view
                    .property::<LocationId>("stop-id")
                    .get();
                let stationboard_id = match stop_id_option {
                    Some(l) => l.id.map(|l| l.parse::<i64>().unwrap_or(0)),
                    None => None,
                };
                let hafas_client = stationboard_main_view
                    .property::<HafasClient>("client")
                    .get();
                let hafas_client_new =
                    HafasClient::new(hafas_client.set_stationboard_id(stationboard_id));
                stationboard_main_view.set_property("client", hafas_client_new);
            });

            obj.connect_notify_local(Some("client"), move |stationboard_main_view, _| {
                let client = stationboard_main_view
                    .property::<HafasClient>("client")
                    .get();

                let products_used = client.stationboard_config.products_used.to_vec();

                for product_used in products_used.iter() {
                    let product_used_inner = *product_used.used.lock().unwrap();
                    if product_used_inner == false {
                        stationboard_main_view
                            .imp()
                            .transportation_means_image
                            .set_icon_name(Some("bus-symbolic-circle"));
                        break;
                    }
                    stationboard_main_view
                        .imp()
                        .transportation_means_image
                        .set_icon_name(Some("bus-symbolic"));
                }
            });

            let stationboard_view = self.stationboard_view.clone();
            self.refresh_stationboard.connect_clicked(move |_| {
                stationboard_view.stationboards();
            });

            self.products_used_popover
                .set_parent(&self.transportation_means.get());
            let products_used_popover = self.products_used_popover.clone();
            self.transportation_means.connect_clicked(move |_| {
                products_used_popover.popup();
            });
        }
    }
    impl WidgetImpl for StationboardMainView {}
    impl BoxImpl for StationboardMainView {}
}

glib::wrapper! {
    pub struct StationboardMainView(ObjectSubclass<imp::StationboardMainView>)
    @extends gtk::Widget, gtk::Box,
    @implements gtk::Accessible, gtk::Buildable, gtk::ConstraintTarget, gtk::Orientable;
}

impl StationboardMainView {
    pub fn new() -> Self {
        glib::Object::new::<Self>(&[])
    }
}
