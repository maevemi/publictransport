// @generated automatically by Diesel CLI.

diesel::table! {
    location_history (id) {
        id -> Integer,
        location_id -> Nullable<Text>,
        location_type -> Nullable<Text>,
        lid -> Text,
        location_name -> Text,
        latitude -> Float,
        longitude -> Float,
        endpoint_name -> Text,
        favorite -> Nullable<Integer>,
    }
}
