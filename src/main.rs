mod application;
mod config;
mod models;
mod schema;
mod widgets;

use self::application::PublictransportApplication;
use self::config::APP_ID;
use self::widgets::window::PublictransportWindow;

use env_logger;
use gettextrs::*;
use gtk::gio;
use gtk::prelude::*;
use once_cell::sync::Lazy;

/// The default tokio runtime to be used for async tasks
pub static RUNTIME: Lazy<tokio::runtime::Runtime> =
    Lazy::new(|| tokio::runtime::Runtime::new().unwrap());

fn init_i18n() -> anyhow::Result<()> {
    setlocale(LocaleCategory::LcAll, "de_DE.utf8");
    bindtextdomain(config::GETTEXT_PACKAGE, config::LOCALEDIR)?;
    textdomain(config::GETTEXT_PACKAGE)?;

    Ok(())
}

fn main() {
    env_logger::init();

    if let Err(err) = init_i18n() {
        log::debug!("Failed to initialize i18n {}", err);
    }

    // Load resources
    let resources =
        gio::Resource::load(config::PKGDATADIR.to_owned() + "/publictransport.gresource")
            .expect("Could not load resources");
    gio::resources_register(&resources);

    // Create a new GtkApplication. The application manages our main loop,
    // application windows, integration with the window manager/compositor, and
    // desktop features such as file opening and single-instance applications.
    let app = PublictransportApplication::new(APP_ID, &gio::ApplicationFlags::empty());

    // Run the application. This function will block until the application
    // exits. Upon return, we have our exit code to return to the shell. (This
    // is the code you see when you do `echo $?` after running a command in a
    // terminal.
    std::process::exit(app.run());
}
