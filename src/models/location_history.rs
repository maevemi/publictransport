use diesel::prelude::*;
use hafas_client::{Coordinates, Location, LocationType};

use crate::models::database::connection;
use crate::schema::location_history::{
    self, endpoint_name, favorite, location_name, location_type,
};

#[derive(Queryable, Clone)]
pub struct LocationHistory {
    pub id: i32,
    pub location_id: Option<String>,
    pub location_type: Option<String>,
    pub lid: String,
    pub location_name: String,
    pub latitude: f32,
    pub longitude: f32,
    pub endpoint_name: String,
    pub favorite: Option<i32>,
}

#[derive(Insertable, Clone)]
#[diesel(table_name = location_history)]
pub struct NewLocationHistory {
    pub location_id: Option<String>,
    pub location_type: Option<String>,
    pub lid: String,
    pub location_name: String,
    pub latitude: f32,
    pub longitude: f32,
    pub endpoint_name: String,
    pub favorite: Option<i32>,
}

impl LocationHistory {
    pub fn delete_location(
        location: &Location,
        endpoint_name_str: &String,
        favorite_bool: bool,
    ) -> anyhow::Result<()> {
        let db = connection();
        let mut connection = db.get()?;

        log::debug!("delete history called");

        diesel::delete(location_history::table)
            .filter(location_name.eq(location.clone().name))
            .filter(endpoint_name.eq(endpoint_name_str))
            .filter(favorite.eq(Some(if favorite_bool { 1 } else { 0 })))
            .execute(&mut connection)?;
        Ok(())
    }

    pub fn delete_locations(favorite_bool: bool) -> anyhow::Result<()> {
        let favorite_int = match favorite_bool {
            true => Some(1),
            false => Some(0),
        };

        let db = connection();
        diesel::delete(location_history::table)
            .filter(favorite.eq(&favorite_int))
            .execute(&mut db.get()?)?;

        Ok(())
    }

    pub fn is_favorite(location: &Location) -> anyhow::Result<bool> {
        let db = connection();
        let result = crate::schema::location_history::dsl::location_history
            .filter(location_name.eq(location.clone().name))
            .limit(1)
            .load::<LocationHistory>(&mut db.get()?)?;

        let mut is_favorite = false;
        if result.len() == 1 {
            if result[0].favorite == Some(1) {
                is_favorite = true;
            }
        }

        Ok(is_favorite)
    }
}

impl NewLocationHistory {
    pub fn add_location(
        location: &Location,
        endpoint_name_str: &String,
        favorite_bool: bool,
    ) -> anyhow::Result<()> {
        let location = location.clone();

        if LocationHistory::is_favorite(&location)? == false {
            let location_history = NewLocationHistory {
                location_id: location.id,
                location_type: location.location_type,
                lid: location.lid,
                location_name: location.name,
                latitude: location.coordinates.latitude as f32,
                longitude: location.coordinates.longitude as f32,
                endpoint_name: endpoint_name_str.clone(),
                favorite: Some(if favorite_bool { 1 } else { 0 }),
            };

            let db = connection();
            let mut connection = db.get()?;

            diesel::delete(location_history::table)
                .filter(location_name.eq(location_history.clone().location_name))
                .filter(favorite.eq(Some(0)))
                .execute(&mut connection)?;

            diesel::insert_into(location_history::table)
                .values(&location_history)
                .execute(&mut connection)?;
        }
        Ok(())
    }

    pub fn locations(
        search_string: String,
        location_type_s: LocationType,
        endpoint_name_str: String,
        favorite_bool: bool,
    ) -> anyhow::Result<Vec<Location>> {
        let db = connection();
        let mut connection = db.get()?;

        let results = match location_type_s {
            LocationType::Station => {
                let result = match favorite_bool {
                    true => crate::schema::location_history::dsl::location_history
                        .filter(location_name.like(search_string + "%"))
                        .filter(endpoint_name.eq(endpoint_name_str))
                        .filter(location_type.eq("S"))
                        .filter(favorite.eq(&Some(1)))
                        .order(location_history::id.desc())
                        .load::<LocationHistory>(&mut connection)?,
                    false => crate::schema::location_history::dsl::location_history
                        .filter(location_name.like(search_string + "%"))
                        .filter(endpoint_name.eq(endpoint_name_str))
                        .filter(location_type.eq("S"))
                        .filter(favorite.eq(&Some(0)))
                        .order(location_history::id.desc())
                        .limit(5)
                        .load::<LocationHistory>(&mut connection)?,
                };
                result
            }
            LocationType::All => {
                let result = match favorite_bool {
                    true => crate::schema::location_history::dsl::location_history
                        .filter(location_name.like(search_string + "%"))
                        .filter(endpoint_name.eq(endpoint_name_str))
                        .filter(favorite.eq(&Some(1)))
                        .order(location_history::id.desc())
                        .load::<LocationHistory>(&mut connection)?,
                    false => crate::schema::location_history::dsl::location_history
                        .filter(location_name.like(search_string + "%"))
                        .filter(endpoint_name.eq(endpoint_name_str))
                        .filter(favorite.eq(&Some(0)))
                        .order(location_history::id.desc())
                        .limit(5)
                        .load::<LocationHistory>(&mut connection)?,
                };
                result
            }
        };

        let mut locations: Vec<Location> = Vec::new();
        for result in results {
            let location = Location {
                id: result.location_id,
                location_type: result.location_type,
                lid: result.lid,
                name: result.location_name,
                coordinates: Coordinates {
                    latitude: result.latitude as f64, // TODO: remove Coodrinates struct in Hafas Client
                    longitude: result.longitude as f64, // TODO: use f32 in hafas client
                },
            };
            locations.push(location);
        }

        Ok(locations)
    }
}
