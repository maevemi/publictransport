use gtk::{glib, subclass::prelude::*};
use hafas_client::Location;
use std::cell::RefCell;

use crate::widgets::map_view::MarkerType;

pub mod imp {
    use super::*;

    #[derive(Debug)]
    pub struct LocationMarker {
        pub place: RefCell<Location>,
        pub marker_type: RefCell<MarkerType>,
    }

    #[glib::object_subclass]
    impl ObjectSubclass for LocationMarker {
        const NAME: &'static str = "LocationMarker";
        type Type = super::LocationMarker;

        fn new() -> Self {
            Self {
                place: RefCell::new(Location::default()),
                marker_type: RefCell::new(MarkerType::Origin),
            }
        }
    }

    impl ObjectImpl for LocationMarker {}
}

glib::wrapper! {
    pub struct LocationMarker(ObjectSubclass<imp::LocationMarker>);
}

impl LocationMarker {
    pub fn new(place: &Location, marker_type: MarkerType) -> Self {
        let location_marker: LocationMarker = glib::Object::new::<LocationMarker>(&[]);
        *location_marker.imp().place.borrow_mut() = place.clone();
        *location_marker.imp().marker_type.borrow_mut() = marker_type;
        location_marker
    }

    pub fn get(&self) -> (Location, MarkerType) {
        let place = &*self.imp().place.borrow();
        let marker_type = &*self.imp().marker_type.borrow();
        (place.clone(), marker_type.clone())
    }
}

impl std::default::Default for LocationMarker {
    fn default() -> Self {
        glib::Object::new::<Self>(&[])
    }
}
