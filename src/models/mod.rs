pub mod client;
pub mod database;
pub mod datetime;
pub mod journey;
pub mod leg;
pub mod location_history;
pub mod location_id;
pub mod location_marker;
pub mod products_used;
pub mod settings;
pub mod stationboard;

use once_cell::sync::Lazy;

use self::settings::Settings;

pub static SETTINGS: Lazy<Settings> = Lazy::new(|| Settings::default());
