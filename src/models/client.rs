use gtk::{glib, subclass::prelude::*};
use hafas_client::Client;
use std::cell::RefCell;

pub mod imp {
    use super::*;

    #[derive(Debug)]
    pub struct HafasClient {
        pub client: RefCell<Client>,
    }

    // necessary because hafas-client does't implement the Default trait for Client
    impl std::default::Default for HafasClient {
        fn default() -> Self {
            HafasClient {
                client: RefCell::new(Client::new(0).unwrap()), // use db client as default
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for HafasClient {
        const NAME: &'static str = "HafasClient";
        type Type = super::HafasClient;

        fn new() -> Self {
            Self {
                client: RefCell::new(Client::new(0).unwrap()),
            }
        }
    }

    impl ObjectImpl for HafasClient {}
}

glib::wrapper! {
    pub struct HafasClient(ObjectSubclass<imp::HafasClient>);
}

impl HafasClient {
    pub fn new(client: &Client) -> Self {
        let hafas_client: HafasClient = glib::Object::new::<HafasClient>(&[]);
        *hafas_client.imp().client.borrow_mut() = client.clone();
        hafas_client
    }

    pub fn get(&self) -> Client {
        let client_ref = self.imp().client.borrow();
        client_ref.clone()
    }
}

impl std::default::Default for HafasClient {
    fn default() -> Self {
        glib::Object::new::<Self>(&[])
    }
}
