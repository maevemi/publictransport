use gtk::{glib, subclass::prelude::*};
use hafas_client::StationBoard;
use std::cell::RefCell;

pub mod imp {
    use super::*;

    #[derive(Debug)]
    pub struct HafasStationboard {
        pub stationboard: RefCell<Option<StationBoard>>,
    }

    // necessary because hafas-client does't implement the Default trait for Stationboard
    impl std::default::Default for HafasStationboard {
        fn default() -> Self {
            HafasStationboard {
                stationboard: RefCell::new(None),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for HafasStationboard {
        const NAME: &'static str = "HafasStationboard";
        type Type = super::HafasStationboard;

        fn new() -> Self {
            Self {
                stationboard: RefCell::new(None),
            }
        }
    }

    impl ObjectImpl for HafasStationboard {}
}

glib::wrapper! {
    pub struct HafasStationboard(ObjectSubclass<imp::HafasStationboard>);
}

impl HafasStationboard {
    pub fn new(stationboard: &StationBoard) -> Self {
        let hafas_stationboard: HafasStationboard = glib::Object::new::<HafasStationboard>(&[]);
        *hafas_stationboard.imp().stationboard.borrow_mut() = Some(stationboard.clone());
        hafas_stationboard
    }

    pub fn get(&self) -> Option<StationBoard> {
        let stationboard_ref = self.imp().stationboard.borrow();
        stationboard_ref.clone()
    }
}

impl std::default::Default for HafasStationboard {
    fn default() -> Self {
        glib::Object::new::<Self>(&[])
    }
}
