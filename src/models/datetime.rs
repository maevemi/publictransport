use chrono::prelude::*;
use gtk::{glib, subclass::prelude::*};
use std::cell::RefCell;

pub mod imp {
    use super::*;

    #[derive(Debug)]
    pub struct DatetimeModel {
        pub datetime: RefCell<TimeType>,
    }

    impl std::default::Default for DatetimeModel {
        fn default() -> Self {
            DatetimeModel {
                datetime: RefCell::new(TimeType::Departure(None)),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for DatetimeModel {
        const NAME: &'static str = "DatetimeModel";
        type Type = super::DatetimeModel;

        fn new() -> Self {
            Self {
                datetime: RefCell::new(TimeType::Departure(None)),
            }
        }
    }

    impl ObjectImpl for DatetimeModel {}
}

glib::wrapper! {
    pub struct DatetimeModel(ObjectSubclass<imp::DatetimeModel>);
}

impl DatetimeModel {
    pub fn new(datetime: TimeType) -> Self {
        let datetime_model: DatetimeModel = glib::Object::new::<DatetimeModel>(&[]);
        *datetime_model.imp().datetime.borrow_mut() = datetime.clone();
        datetime_model
    }

    pub fn get(&self) -> TimeType {
        let datetime = self.imp().datetime.borrow();
        datetime.clone()
    }
}

#[derive(Debug, Clone)]
pub enum TimeType {
    Departure(Option<DateTime<Local>>),
    Arrival(Option<DateTime<Local>>),
}

impl std::default::Default for DatetimeModel {
    fn default() -> Self {
        glib::Object::new::<Self>(&[])
    }
}
