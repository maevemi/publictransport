use gtk::{glib, subclass::prelude::*};
use hafas_client::{Client, ProductUsed};
use std::cell::RefCell;

pub mod imp {
    use super::*;

    #[derive(Debug)]
    pub struct ProductsUsedModel {
        pub products_used: RefCell<Vec<ProductUsed>>,
    }

    impl std::default::Default for ProductsUsedModel {
        fn default() -> Self {
            ProductsUsedModel {
                products_used: RefCell::new(Client::new(0).unwrap().journeys_config.products_used), // use db client as default
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for ProductsUsedModel {
        const NAME: &'static str = "ProductsUsedModel";
        type Type = super::ProductsUsedModel;

        fn new() -> Self {
            Self {
                products_used: RefCell::new(Client::new(0).unwrap().journeys_config.products_used), // use db client as default
            }
        }
    }

    impl ObjectImpl for ProductsUsedModel {}
}

glib::wrapper! {
    pub struct ProductsUsedModel(ObjectSubclass<imp::ProductsUsedModel>);
}

impl ProductsUsedModel {
    pub fn new(products_used: &Vec<ProductUsed>) -> Self {
        let products_used_model: ProductsUsedModel = glib::Object::new::<ProductsUsedModel>(&[]);
        *products_used_model.imp().products_used.borrow_mut() = products_used.clone();
        products_used_model
    }

    pub fn get(&self) -> Vec<ProductUsed> {
        let products_used = self.imp().products_used.borrow();
        products_used.clone()
    }
}

impl std::default::Default for ProductsUsedModel {
    fn default() -> Self {
        glib::Object::new::<Self>(&[])
    }
}
