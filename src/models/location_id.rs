use gtk::{glib, subclass::prelude::*};
use hafas_client::Location;
use std::cell::RefCell;

pub mod imp {
    use super::*;

    #[derive(Debug)]
    pub struct LocationId {
        pub location_id: RefCell<Option<Location>>,
    }

    impl std::default::Default for LocationId {
        fn default() -> Self {
            LocationId {
                location_id: RefCell::new(None),
            }
        }
    }

    #[glib::object_subclass]
    impl ObjectSubclass for LocationId {
        const NAME: &'static str = "LocationId";
        type Type = super::LocationId;

        fn new() -> Self {
            Self {
                location_id: RefCell::new(None),
            }
        }
    }

    impl ObjectImpl for LocationId {}
}

glib::wrapper! {
    pub struct LocationId(ObjectSubclass<imp::LocationId>);
}

impl LocationId {
    pub fn new(id: Option<Location>) -> Self {
        let location_id: LocationId = glib::Object::new::<LocationId>(&[]);
        *location_id.imp().location_id.borrow_mut() = id.clone();
        location_id
    }

    pub fn get(&self) -> Option<Location> {
        let location_id = &*self.imp().location_id.borrow();
        location_id.clone()
    }
}

impl std::default::Default for LocationId {
    fn default() -> Self {
        glib::Object::new::<Self>(&[])
    }
}
