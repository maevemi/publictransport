use std::ops::Deref;

use gtk::gio;

use crate::config;

pub struct Settings(gio::Settings);

impl Default for Settings {
    fn default() -> Self {
        Self(gio::Settings::new(config::APP_ID))
    }
}

impl Deref for Settings {
    type Target = gio::Settings;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

unsafe impl Send for Settings {}
unsafe impl Sync for Settings {}
