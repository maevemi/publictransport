use gettextrs::gettext;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{gio, glib};

use crate::config;
use crate::widgets::preferences::PreferencesWindow;
use crate::PublictransportWindow;

mod imp {
    use super::*;

    #[derive(Debug, Default)]
    pub struct PublictransportApplication {}

    #[gtk::glib::object_subclass]
    impl ObjectSubclass for PublictransportApplication {
        const NAME: &'static str = "PublictransportApplication";
        type Type = super::PublictransportApplication;
        type ParentType = gtk::Application;
    }

    impl ObjectImpl for PublictransportApplication {
        fn constructed(&self) {
            let obj = self.obj();

            self.parent_constructed();

            obj.setup_gactions();
            obj.set_accels_for_action("app.quit", &["<primary>q"]);
            obj.set_accels_for_action("win.show-help-overlay", &["<primary>question"]);
            obj.set_accels_for_action("app.preferences", &["<primary>comma"]);

            // register custom subclasses
            crate::widgets::locationsearch::LocationSearch::static_type();
            crate::widgets::journeys::mainview::JourneysMainView::static_type();
            crate::widgets::list_view::ListView::static_type();
            crate::widgets::journeys::journeys_view::JourneysView::static_type();
            crate::widgets::journeys::detailsview::JourneysDetailsView::static_type();
            crate::widgets::stationboard::stationboard_view::StationboardView::static_type();
            crate::widgets::datetime::button::DatetimeButton::static_type();
            crate::widgets::datetime::view::DateTimeView::static_type();
        }
    }

    impl ApplicationImpl for PublictransportApplication {
        // We connect to the activate callback to create a window when the application
        // has been launched. Additionally, this callback notifies us when the user
        // tries to launch a "second instance" of the application. When they try
        // to do that, we'll just present any existing window.
        fn activate(&self) {
            let application = self.obj();
            // Get the current window or create one if necessary
            let window = if let Some(window) = application.active_window() {
                window
            } else {
                let window = PublictransportWindow::new(&*application);
                window.set_default_size(400, 700);
                window.upcast()
            };

            // Ask the window manager/compositor to present the window
            window.present();
        }

        fn startup(&self) {
            self.parent_startup();

            adw::init().expect("failed to init libadwaita");

            if let Some(ref display) = gtk::gdk::Display::default() {
                let p = gtk::CssProvider::new();
                gtk::CssProvider::load_from_resource(
                    &p,
                    "/com/gitlab/maevemi/publictransport/style.css",
                );
                gtk::StyleContext::add_provider_for_display(display, &p, 5);
                let theme = gtk::IconTheme::for_display(display);
                theme.add_resource_path("/com/gitlab/maevemi/publictransport/icons/");
            }
        }
    }

    impl GtkApplicationImpl for PublictransportApplication {}
}

glib::wrapper! {
    pub struct PublictransportApplication(ObjectSubclass<imp::PublictransportApplication>)
    @extends gio::Application, gtk::Application,
    @implements gio::ActionGroup, gio::ActionMap;
}

impl PublictransportApplication {
    pub fn new(application_id: &str, flags: &gio::ApplicationFlags) -> Self {
        log::info!("Public Transport ({})", config::APP_ID);
        log::info!("Version: {}", config::VERSION);
        log::debug!("Datadir: {}", config::PKGDATADIR);

        glib::Object::new::<Self>(&[("application-id", &application_id), ("flags", flags)])
    }

    fn setup_gactions(&self) {
        let preferences_action = gio::ActionEntry::builder("preferences")
            .activate(|app: &Self, _, _| {
                let window = app.active_window().unwrap();
                let preferences = PreferencesWindow::new();
                preferences.set_transient_for(Some(&window));
                preferences.present();
            })
            .build();

        let quit_action = gio::ActionEntry::builder("quit")
            .activate(|app: &Self, _, _| {
                app.quit();
            })
            .build();

        let about_action = gio::ActionEntry::builder("about")
            .activate(|app: &Self, _, _| {
                app.show_about();
            })
            .build();

        self.add_action_entries([preferences_action, quit_action, about_action])
            .unwrap();
    }

    fn show_about(&self) {
        let window = self.active_window().unwrap();
        let dialog = adw::AboutWindow::builder()
            .transient_for(&window)
            .modal(true)
            .application_name("Public Transport")
            .version(config::VERSION)
            .comments(&gettext(
                "public transportation app (currently) for Germany",
            ))
            .website("https://gitlab.com/maevemi/publictransport")
            .developers(vec!["Maeve".into()])
            .application_icon(config::APP_ID)
            .license_type(gtk::License::Gpl30)
            .build();

        dialog.present();
    }
}
