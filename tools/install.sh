#!/bin/bash
# install flatpak to local system

cd ..
flatpak-builder --repo=myx86repo --force-clean x86flatpak build-aux/com.gitlab.maevemi.publictransport.Devel.json
flatpak build-bundle myx86repo publictransport_x86.flatpak com.gitlab.maevemi.publictransport.Devel
flatpak install --user publictransport_x86.flatpak
