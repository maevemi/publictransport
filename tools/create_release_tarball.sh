#!/bin/bash

# Updateflow:

# - add release notes to com.gitlab.maevemi.publictransport.metainfo.xml.in
# - update version in cargo.toml, meson-build, 
# - generate new po files: xgettext -f po/POTFILES.in -o po/messages.pot, msgmerge --update de.po messages.pot, (include in commit hook)
# - commit

GNOME_RELEASE_RUNTIME=43
FLATPAK_MODULE=publictransport
FLATPAK_ID=com.gitlab.maevemi.publictransport
MANIFEST_PATH=com.gitlab.maevemi.publictransport.json

set -e

# check if versions of cargo and meson match
CARGO_VERSION=`cat ../Cargo.toml | awk -F '["]' 'NR == 3 {print $2}'`
MESON_VERSION=`cat ../meson.build | awk -F "[']" 'NR == 2 {print $2}'`
if [ $CARGO_VERSION != $MESON_VERSION ];
then
    echo 'meson and cargo versions do not match'
    exit 0
fi

# create release manifest
cd ../build-aux
cp com.gitlab.maevemi.publictransport.Devel.json $MANIFEST_PATH
echo "$(jq --arg FLATPAK_ID "$FLATPAK_ID" '."app-id"=$FLATPAK_ID' $MANIFEST_PATH)" > $MANIFEST_PATH
echo "$(jq --arg GNOME_RELEASE_RUNTIME "$GNOME_RELEASE_RUNTIME" '."runtime-version"=$GNOME_RELEASE_RUNTIME' $MANIFEST_PATH)" > $MANIFEST_PATH
# for some reason this doesn't work in one line
DUMMY=`jq 'del(."finish-args"[] | select(. == "--env=RUST_LOG=publictransport=debug,"))' $MANIFEST_PATH`
echo $DUMMY | jq . > $MANIFEST_PATH 
echo "$(jq 'del(.modules[1]."config-opts")' $MANIFEST_PATH)" > $MANIFEST_PATH
sed -i 's/"run-tests": true,/"builddir" : true,/' $MANIFEST_PATH # TODO use jq

# Build tarball with vendored rust dependencies
flatpak-builder --keep-build-dirs --force-clean --user --disable-rofiles-fuse --stop-at=${FLATPAK_MODULE} flatpak_app --repo=repo ${MANIFEST_PATH}
echo "meson dist --allow-dirty" | flatpak-builder --disable-rofiles-fuse --build-shell=${FLATPAK_MODULE} flatpak_app ${MANIFEST_PATH}

# create tag
echo `git tag -a "v$CARGO_VERSION" -m "v$CARGO_VERSION"`

# Upload release-tarball
cp ../build-aux/.flatpak-builder/build/${FLATPAK_MODULE}/_flatpak_build/meson-dist/publictransport-$CARGO_VERSION.tar.xz ../../publictransport-releases/releases/
cd ../../publictransport-releases
git add releases/publictransport-$CARGO_VERSION.tar.xz
git commit -m "add new release"
git push

# # release Version on gitlab and add release notes/sha file

# create and push flathub manifest
URL_VARIABLE=https://gitlab.com/maevemi/publictransport-releases/-/raw/main/releases/publictransport-$CARGO_VERSION.tar.xz
SHA_SUM=`cat ../publictransport/build-aux/.flatpak-builder/build/publictransport/_flatpak_build/meson-dist/publictransport-$CARGO_VERSION.tar.xz.sha256sum | awk -F '[ ]' '{print $1}'`
cd ../com.gitlab.maevemi.publictransport
cp ../publictransport/build-aux/com.gitlab.maevemi.publictransport.json $MANIFEST_PATH
echo "$(jq 'del(."build-options"."build-args")' $MANIFEST_PATH)" > $MANIFEST_PATH
echo "$(jq 'del(.modules[1].sources[0].path)' $MANIFEST_PATH)" > $MANIFEST_PATH
echo "$(jq '.modules[1].sources[0].type="archive"' $MANIFEST_PATH)" > $MANIFEST_PATH
echo "$(jq --arg URL_VARIABLE "$URL_VARIABLE" '.modules[1].sources[0].url=$URL_VARIABLE' $MANIFEST_PATH)" > $MANIFEST_PATH
echo "$(jq --arg SHA_SUM "$SHA_SUM" '.modules[1].sources[0].sha256=$SHA_SUM' $MANIFEST_PATH)" > $MANIFEST_PATH
git add $MANIFEST_PATH
git commit -m "add new release"
git pull
git push

# # create merge request against flathub and if pipeline passes merge
echo $URL_VARIABLE
