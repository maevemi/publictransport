CREATE TABLE location_history (
  id INTEGER NOT NULL PRIMARY KEY,
  location_id VARCHAR,
  location_type VARCHAR,
  lid VARCHAR NOT NULL,
  location_name VARCHAR NOT NULL,
  latitude FLOAT NOT NULL,
  longitude FLOAT NOT NULL,
  endpoint_name VARCHAR NOT NULL,
  favorite INTEGER
)